﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Smoothics.LicenseManagement.Core;
using Microsoft.SharePoint.Administration;
using System.Drawing;

namespace Smoothics.LicenseManagement.Layouts.Smoothics.LicenseManagement
{
    public partial class LicenseDetails : LayoutsPageBase
    {
        #region FeatureId

        public Guid FeatureId
        {
            get
            {
                if (ViewState["FeatureId"] == null)
                    return Guid.Empty;
                return (Guid) ViewState["FeatureId"];
            }
            set
            {
                ViewState["FeatureId"] = value;
            }
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FeatureId = new Guid(Request.QueryString["featureId"]);
                LabelFarmId.Text = SPFarm.Local.Id.ToString();
                LabelServerId.Text = SPServer.Local.Id.ToString();
                UpdateIsValidField(LicenseManager.Instance.Features[FeatureId].IsValid);
            }
        }

        protected void ButtonVerifyLicenseKeyAndSignature_Click(object sender, EventArgs e)
        {
            var licenseKey = TextBoxLicenseKey.Text.Trim();

            //TODO: convert base64string, extract key anf signature and verify

            //var result = LicenseManager.Instance.VerifyAndUpdateLicense(FeatureId, licenseKey, signature);
            //UpdateIsValidField(result);
        }

        private void UpdateIsValidField(bool result)
        {
            // TODO: i18n or set icon
            LabelIsValidValue.ForeColor = result ? Color.Green : Color.Red;
            LabelIsValidValue.Text = result.ToString();
        }
    }
}
