﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LicenseManagementCenter.aspx.cs" Inherits="Smoothics.LicenseManagement.Layouts.Smoothics.LicenseManagement.LicenseManagementCenter" DynamicMasterPageFile="~masterurl/default.master" %>

<%@ Register TagPrefix="spuc" TagName="ButtonSection" Src="~/_controltemplates/15/ButtonSection.ascx" %>
<%@ Register TagPrefix="spuc" TagName="InputFormSection" Src="~/_controltemplates/15/InputFormSection.ascx" %>
<%@ Register TagPrefix="spuc" TagName="InputFormControl" Src="~/_controltemplates/15/InputFormControl.ascx" %>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <script type="text/javascript">

        function openDialog(featureId, featureName) {
            var options = {
                title: "License Details: " + featureName,
                allowMaximize: false,
                showClose: true,
                width: 600,
                height: 400,
                url: "/_layouts/15/Smoothics.LicenseManagement/LicenseDetails.aspx?featureId=" + featureId,
                dialogReturnValueCallback: Function.createDelegate(null, function (result, returnValue) {
                    alert(result);
                    alert(returnValue);
                    if (result == SP.UI.DialogResult.OK) {
                        if (returnValue == null) {
                            SP.UI.Notify.addNotification('Operation successful');
                            SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
                        } else {
                            location.href = returnValue;
                        }
                    }
                })
            }
            //SP.UI.ModalDialog.showModalDialog(options);
            SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog', options);
            return false;   //no postback
        };

    </script>


    <table border="0" cellspacing="0" cellpadding="0" width="100%">

        <spuc:InputFormSection runat="server" Title="<% $Resources:Smoothics.LicenseManagement,LicenseManagementVersionTitle %>">
            <template_description>
                <asp:Literal ID="Literal3" runat="server" text="<% $Resources:Smoothics.LicenseManagement,LicenseManagementVersionDescription %>"/>
            </template_description>
            <template_inputformcontrols>
                <spuc:InputFormControl runat="server">
                    <Template_Control>
                        <asp:Label ID="LabelLicenseManagementVersion" runat="server"></asp:Label>
                    </Template_Control>
                </spuc:InputFormControl>
            </template_inputformcontrols>
        </spuc:InputFormSection>

        <spuc:InputFormSection runat="server" Title="<% $Resources:Smoothics.LicenseManagement,SharePointBuildVersionTitle %>">
            <template_description>
                <asp:Literal ID="LiteralSharePointDescription" runat="server" text="<% $Resources:Smoothics.LicenseManagement,SharePointBuildVersionDescription %>"/>
            </template_description>
            <template_inputformcontrols>
                <spuc:InputFormControl runat="server">
                    <Template_Control>
                        <asp:Label ID="LabelSharePointVersion" runat="server"></asp:Label>
                    </Template_Control>
                </spuc:InputFormControl>
            </template_inputformcontrols>
        </spuc:InputFormSection>

        <spuc:InputFormSection runat="server" Title="<% $Resources:Smoothics.LicenseManagement,ServerIdTitle %>">
            <template_description>
                <asp:Literal ID="Literal2" runat="server" text="<% $Resources:Smoothics.LicenseManagement,ServerIdDescription %>"/>
            </template_description>
            <template_inputformcontrols>
                <spuc:InputFormControl runat="server">
                    <Template_Control>
                        <asp:Label ID="LabelServerId" runat="server"></asp:Label>
                    </Template_Control>
                </spuc:InputFormControl>
            </template_inputformcontrols>
        </spuc:InputFormSection>

        <spuc:InputFormSection runat="server" Title="<% $Resources:Smoothics.LicenseManagement,FarmIdTitle %>">
            <template_description>
                <asp:Literal ID="Literal1" runat="server" text="<% $Resources:Smoothics.LicenseManagement,FarmIdDescription %>"/>
            </template_description>
            <template_inputformcontrols>
                <spuc:InputFormControl runat="server">
                    <Template_Control>
                        <asp:Label ID="LabelFarmId" runat="server"></asp:Label>
                    </Template_Control>
                </spuc:InputFormControl>
            </template_inputformcontrols>
        </spuc:InputFormSection>

        <spuc:InputFormSection runat="server" Title="<% $Resources:Smoothics.LicenseManagement,InstalledFeatureTitle %>">
            <template_description>
                <asp:Literal ID="Literal4" runat="server" text="<% $Resources:Smoothics.LicenseManagement,InstalledFeatureDescription %>"/>
            </template_description>
            <template_inputformcontrols>
                <spuc:InputFormControl runat="server">
                    <Template_Control>

            <asp:GridView ID="GridView1" runat="server"
                DataKeyNames="FeatureId, FeatureTitle"
                AutoGenerateColumns="False"
                CellPadding="5"
                OnRowDataBound="GridView1_RowDataBound"
                HeaderStyle-BackColor="LightGray"
                >
                <Columns>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="Feature"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("FeatureTitle") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="Description"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("FeatureDescription") %>' Width="320px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="Valid Key?"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Eval("IsValid") %>' Enabled="false"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Details"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

                    </Template_Control>
                </spuc:InputFormControl>
            </template_inputformcontrols>
        </spuc:InputFormSection>

    </table>

</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Smoothics Solutions
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    License Management Center
</asp:Content>
