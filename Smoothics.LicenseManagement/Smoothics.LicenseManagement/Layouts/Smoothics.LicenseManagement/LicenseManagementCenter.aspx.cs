﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Administration;
using Smoothics.LicenseManagement.Core;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Smoothics.LicenseManagement.Layouts.Smoothics.LicenseManagement
{
    public partial class LicenseManagementCenter : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var farmBuildVersion = SPFarm.Local.BuildVersion;
                LabelSharePointVersion.Text = farmBuildVersion.ToString();
                LabelFarmId.Text = SPFarm.Local.Id.ToString();
                LabelServerId.Text = SPServer.Local.Id.ToString();
                LabelLicenseManagementVersion.Text = LicenseManager.Instance.Version;

                BindAllFeaturesToGridView();
            }
        }

        private void BindAllFeaturesToGridView()
        {
            var enumerator = LicenseManager.Instance.Features.Values.GetEnumerator();
            var list = new List<LicenseData>();
            while (enumerator.MoveNext())
            {
                list.Add(enumerator.Current);
            }

            //TODO: remove
            if (list.Count == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    list.Add(new LicenseData()
                    {
                        FeatureTitle = $"Testfeature {i}",
                        FeatureDescription= "This is a short and concise description about the feature. Maybe we should also provide a link to our product with its installation/usage instructions (but what about intranet without internet access?).",
                        IsValid = i % 2 == 0,
                        FeatureId = Guid.NewGuid()
                    });
                    LicenseManager.Instance.Features.Add(list[i].FeatureId, list[i]);
                }
            }

            GridView1.DataSource = list;
            GridView1.DataBind();
        }

        protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var title = GridView1.DataKeys[e.Row.DataItemIndex].Values["FeatureTitle"] as string;
                var featureId = GridView1.DataKeys[e.Row.DataItemIndex].Values["FeatureId"] as Guid?;
                var linkControl = e.Row.FindControl("LinkButton1") as LinkButton;
                if (linkControl != null)
                {
                    linkControl.OnClientClick = $"return openDialog('{featureId}', '{title}')";
                    linkControl.Attributes.Add("onClick", "return false;"); //no postback!
                }
            }
        }
    }
}
