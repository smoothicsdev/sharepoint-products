﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LicenseDetails.aspx.cs" Inherits="Smoothics.LicenseManagement.Layouts.Smoothics.LicenseManagement.LicenseDetails" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <div style="float: left;">
        <asp:Label ID="LabelIsValidText" runat="server" Width="80px" Text="<% $Resources:Smoothics.LicenseManagement,IsValidTitle %>"></asp:Label>
        <asp:Label ID="LabelIsValidValue" runat="server"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 3px;"></div>

    <div style="float: left;">
        <asp:Label ID="LabelServerText" runat="server" Width="80px" Text="<% $Resources:Smoothics.LicenseManagement,ServerIdTitle %>"></asp:Label>
        <asp:Label ID="LabelServerId" runat="server"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 3px;"></div>

    <div style="float: left;">
        <asp:Label ID="LabelFarmText" runat="server" Width="80px" Text="<% $Resources:Smoothics.LicenseManagement,FarmIdTitle %>"></asp:Label>
        <asp:Label ID="LabelFarmId" runat="server"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 3px;"></div>

    <div style="float: left;">
        <asp:Label ID="LabelCompanyText" runat="server" Width="80px" Text="<% $Resources:Smoothics.LicenseManagement,CompanyTitle %>"></asp:Label>
        <asp:Label ID="LabelCompanyValue" runat="server"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>

    <div style="margin-bottom: 5px;">
        <asp:Label ID="LabelLicenseKeyText" runat="server" Width="80px" Text="<% $Resources:Smoothics.LicenseManagement,LicenseKeyTitle %>"></asp:Label><br />
        <asp:TextBox ID="TextBoxLicenseKey" runat="server" Rows="12" Columns="90" TextMode="MultiLine" Wrap="True"></asp:TextBox>
    </div>

    <div style="text-align: center">
        <asp:Button ID="ButtonVerifyLicenseKey" Text="<% $Resources:Smoothics.LicenseManagement,ButtonVerifyLicenseKey %>" runat="server" OnClick="ButtonVerifyLicenseKeyAndSignature_Click" />
    </div>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
</asp:Content>
