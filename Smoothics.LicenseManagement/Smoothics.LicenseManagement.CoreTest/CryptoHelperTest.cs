﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace Smoothics.LicenseManagement.CoreTest
{
    [TestClass]
    public class CryptoHelperTest
    {
        [TestMethod]
        public void CanCreateKeyPairFiles()
        {
            CryptoHelper helper = new CryptoHelper();

            helper.CreateNewKeyPair("smoothics");

            try
            {
                using (var stream = File.Open("smoothics.pubKey", FileMode.Open))
                {
                    stream.Close();
                }

                using (var stream = File.Open("smoothics.privKey", FileMode.Open))
                {
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void CanEncryptWithPublicKey_And_DecryptWithPrivateKey()
        {
            CryptoHelper privateHelper = new CryptoHelper("smoothics.privKey");
            CryptoHelper publicHelper = new CryptoHelper("smoothics.pubKey");
            var testStr = "Teststring";

            var encryptedStr = publicHelper.Encrypt(testStr);
            var decryptedStr = privateHelper.Decrypt(encryptedStr);

            Assert.AreNotEqual(testStr, encryptedStr);
            Assert.AreEqual(testStr, decryptedStr);
        }

        [TestMethod]
        public void CanSignWithPrivateKey_And_VerifyWithPublicKey()
        {
            CryptoHelper privateHelper = new CryptoHelper("smoothics.privKey");
            CryptoHelper publicHelper = new CryptoHelper("smoothics.pubKey");
            var testStr = "Teststring";

            var signature = privateHelper.Sign(testStr);
            var result = publicHelper.Verify(testStr, signature);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CanDetectInvalidSignature()
        {
            CryptoHelper privateHelper = new CryptoHelper("smoothics.privKey");
            CryptoHelper publicHelper = new CryptoHelper("smoothics.pubKey");
            var testStr = "Teststring";

            var signature = privateHelper.Sign(testStr);
            var result = publicHelper.Verify("TestString", signature);

            Assert.IsFalse(result);
        }
    }
}
