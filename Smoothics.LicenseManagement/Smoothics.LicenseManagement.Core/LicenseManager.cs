﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Smoothics.LicenseManagement.Core
{
    public sealed class LicenseManager
    {
        private static readonly LicenseManager instance = new LicenseManager();
        private SHA1CryptoServiceProvider signingProvider = new SHA1CryptoServiceProvider();
        CryptoHelper cryptoHelper = new CryptoHelper("smoothics.pubKey", true);    //embedded resource!
        public Dictionary<Guid, LicenseData> Features;

        private string _version = null;
        /// <summary>
        /// LicenseManager Version. In case of an Intranet SharePoint without www-access the admins could see if they have the most recent 
        /// manager installed (listing of new features).
        /// </summary>
        public string Version
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_version))
                {
                    _version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                }
                return _version;
            }
        }

        static LicenseManager()
        {
        }

        private LicenseManager()
        {
            // hardcoded ApplicationId => open solution of certain feature (i.e. FileUploadRestrictions) 
            // => project properties => Assembly Information

            //TODO: ALL feature implementation should get this license manager and register themselves here!
            Features = new Dictionary<Guid, LicenseData>();
        }

        public static LicenseManager Instance
        {
            get
            {
                return instance;
            }
        }

        public bool VerifyAndUpdateLicense(Guid featureId, string licenseKey, string signature)
        {
            if (string.IsNullOrWhiteSpace(licenseKey) || string.IsNullOrWhiteSpace(signature))
                return false;

            var isValid = cryptoHelper.Verify(licenseKey, signature);
            Features[featureId].IsValid = isValid;
            if (isValid)
            {
                //TODO: extract data from license key string and update LicenseData object
                Features[featureId].LicenseString = licenseKey;
                Features[featureId].Signature = signature;
            }

            return isValid;
        }
    }
}
