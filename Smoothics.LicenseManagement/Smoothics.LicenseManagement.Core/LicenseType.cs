﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smoothics.LicenseManagement.Core
{
    public enum LicenseType
    {
        Free,
        Trial,
        Server,
        Farm
    }
}
