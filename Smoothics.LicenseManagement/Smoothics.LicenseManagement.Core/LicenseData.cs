﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smoothics.LicenseManagement.Core
{
    public class LicenseData
    {
        public bool IsValid { get; set; }
        public string LicenseFeatureAssemblyVersion { get; set; }
        public LicenseType LicenseType { get; set; }
        public string LicenseString { get; set; }
        public string Signature { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public Guid FeatureId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid? FarmId { get; set; }
        public Guid? ServerId { get; set; }
        public string RawBase64Data { get; set; }
        public string FeatureTitle { get; set; }
        public string FeatureDescription { get; set; }

        public LicenseData() : this(string.Empty)
        {
        }

        public LicenseData(string base64DataString)
        {
            // TODO differentiation per license version?
            RawBase64Data = base64DataString;
        }

        public string ToBase64DataString()
        {
            var str = string.Join(";", FeatureId,
                                        CustomerId,
                                        FarmId,
                                        ServerId,
                                        LicenseString,
                                        Signature,
                                        LicenseType,
                                        CompanyName,
                                        Email,
                                        LicenseFeatureAssemblyVersion,
                                        ExpirationDate.ToShortDateString());
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(str));
        }
    }
}
