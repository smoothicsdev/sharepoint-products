﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace Smoothics.LicenseManagement
{
    public class CryptoHelper
    {
        private RSACryptoServiceProvider rsa = null;
        private SHA1CryptoServiceProvider signingProvider = new SHA1CryptoServiceProvider();

        public CryptoHelper()
        {
            rsa = new RSACryptoServiceProvider();
        }

        /// <summary>
        /// In Production, we provide our public key as embedded resource.
        /// In (Unit) Testing, we provide generated keys (see CoreTest).
        /// </summary>
        /// <param name="keyfilePath"></param>
        /// <param name="isEmbeddedResource"></param>
        public CryptoHelper(string keyfilePath, bool isEmbeddedResource = false) : this()
        {
            if (string.IsNullOrWhiteSpace(keyfilePath))
                throw new Exception("Please provide the path to an existing keyfile.");

            if (isEmbeddedResource)
            {
                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = $"Smoothics.LicenseManagement.Core.{keyfilePath}";
                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                    using (StreamReader sr = new StreamReader(stream))
                    {
                        string keytext = sr.ReadToEnd();
                        rsa.FromXmlString(keytext);
                    }
            }
            else
            {
                using (StreamReader sr = new StreamReader(keyfilePath))
                {
                    string keytext = sr.ReadToEnd();
                    rsa.FromXmlString(keytext);
                }
            }
        }

        public void CreateNewKeyPair(string keyFileName)
        {
            var pathPublic = Path.Combine(Environment.CurrentDirectory, string.Format("{0}.pubKey", keyFileName));
            var pathPrivate = Path.Combine(Environment.CurrentDirectory, string.Format("{0}.privKey", keyFileName));

            using (StreamWriter sw = new StreamWriter(pathPublic, false, Encoding.UTF8))
            {
                sw.Write(rsa.ToXmlString(false));
            }

            using (StreamWriter sw = new StreamWriter(pathPrivate, false, Encoding.UTF8))
            {
                sw.Write(rsa.ToXmlString(true));
            }
        }

        /// <summary>
        /// Use this if someone wants to send YOU an encrypted message.
        /// </summary>
        /// <param name="plainString"></param>
        /// <returns></returns>
        public string Encrypt(string plainString)
        {
            var val = Convert.ToBase64String(rsa.Encrypt(Encoding.UTF8.GetBytes(plainString), false));
            return val;
        }

        /// <summary>
        /// Use this if someone has sent YOU an encrypted message (sender uses your public key 
        /// to encrypt the message to you) and you want to read it.
        /// </summary>
        /// <param name="encryptedString"></param>
        /// <returns></returns>
        public string Decrypt(string encryptedString)
        {
            var val = rsa.Decrypt(Convert.FromBase64String(encryptedString), false);
            var str = Encoding.UTF8.GetString(val);
            return str;
        }

        /// <summary>
        /// Uniquely sign data.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>Signature for param 'data'</returns>
        public string Sign(string data)
        {
            var signatureBytes = rsa.SignData(Encoding.UTF8.GetBytes(data), signingProvider);
            return Convert.ToBase64String(signatureBytes);
        }

        /// <summary>
        /// Verify data with signature on the recipient side. 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        public bool Verify(string data, string signature)
        {
            var signatureBytes = Convert.FromBase64String(signature);
            var result = rsa.VerifyData(Encoding.UTF8.GetBytes(data), signingProvider, signatureBytes);
            return result;
        }
    }
}
