﻿function Deploy-Solution
{
    param(
        [Parameter(Mandatory=$true)]
        [string]$ConfigurationFile
    )

    [xml]$xml = Get-Content -LiteralPath $ConfigurationFile

    $powershellSnapin = "Microsoft.Sharepoint.Powershell"
    if ((Get-PSSnapin -Name $powershellSnapin -ErrorAction SilentlyContinue) -eq $null )
    {
        Write-Host "Powershell Snapin '$($powershellSnapin) missing. Adding it now."
        Add-PsSnapin $powershellSnapin
        Write-Host "Done"
    }

    $all = $xml.configuration.Solutions.SelectNodes('Solution') | select Path,WebApplications
    Write-Output "$($all.Count) solution(s) found!"
    foreach($solution in $all)
    {
        $webApps = $solution.WebApplications.SelectNodes('WebApplication')
        Write-Output "+ Solution: $($solution.Path), WebApplications: $($webApps.Count)"
        $webAppVals = $webApps | select Url,Force,Features
        Write-Output "  --- UNINSTALL FEATURES ---"
        DoUninstall-SPSolution -SolutionPath $solution.Path -WebApplications $webAppVals
        Write-Output "  --- INSTALL FEATURES ---"
        DoInstall-SPSolution -SolutionPath $solution.Path -WebApplications $webAppVals
        Write-Output "  Done"
    }
    Write-Output "+ Deployment finished."
}

function DoUninstall-SPSolution
{
    param(
    [string]$SolutionPath,
    [object[]]$WebApplications
    )

    foreach($webApp in $WebApplications)
    {
        $features = $webApp.Features.SelectNodes('Feature')
        Write-Output "   WebApp Url: $($webApp.Url), Force: $($webApp.Force), Features: $($features.Count)"
        DoUninstall-SPFeatures -WebAppUrl $webApp.Url -Force $webApp.Force -Features $features

        $solutionPackageAlreadyExists = Get-SPSolution -Identity $(Get-Childitem $SolutionPath).Name -ErrorAction SilentlyContinue
        if($solutionPackageAlreadyExists -ne $null)
        {
            if($webApp.Url -ne $null)
            {
                # WebApplication
                Write-Output "     Uninstall solution $($solutionPackageAlreadyExists.Name) from $($webApp.Url)"
                Uninstall-SPSolution $solutionPackageAlreadyExists -confirm:$false –WebApplication $webApp.Url -ErrorAction SilentlyContinue
            }
            else
            {
                # Globally
                Write-Output "     Uninstall solution $($solutionPackageAlreadyExists.Name) globally"
                Uninstall-SPSolution $solutionPackageAlreadyExists -AllWebApplications -ErrorAction SilentlyContinue
            }
            while($solutionPackageAlreadyExists.JobExists)
            {
                Write-Output "       Uninstall in progress..."
                Start-Sleep 5
            }

            Write-Output "     Removing Solution $($solutionPackageAlreadyExists.Name) on $($webApp.Url)"
            do
            {
                try
                {
                    Remove-SPSolution $solutionPackageAlreadyExists -confirm:$false -ErrorAction SilentlyContinue
                    write-host "       Verifying if the solution is removed. Please wait..."
                    $solutionObject = Get-SPSolution -Identity $solutionPackageAlreadyExists.Name -ErrorAction SilentlyContinue
                    Start-Sleep -Seconds 5;
                }
                catch
                { }
            } while($solutionObject);
            Write-Output "     Done!"
        } else 
        {
            Write-Output "     Solution $($SolutionPath) doesn't exist on $($webApp.Url) - Nothing todo"
        }
    }
}

function DoUninstall-SPFeatures
{
    param(
        [string]$WebAppUrl,
        [string]$Force,
        [object[]]$Features
    )

    foreach($feature in $Features)
    {
        $urls = $feature.SelectNodes('Url')
        Write-Output "     WebApp Url: $($webAppUrl), Feature Id: $($feature.Id), Feature Force: $($feature.Force), Urls: $($urls.Count)"
        DoDisable-SPFeature -WebAppUrl $webAppUrl -Id $feature.Id -Force $feature.Force -Urls $urls
    }
}

function DoDisable-SPFeature
{
    param(
        [string]$WebAppUrl,
        [string]$Id,
        [string]$Force,
        [object[]]$Urls
    )

    if($Urls.Count -gt 0)
    {
        $installedFeature = Get-SPFeature | Where {$_.Id -eq $Id}
        if($installedFeature -ne $null)
        {
            Write-Output "       Found installed Feature: $($installedFeature.DisplayName). Trying to disable and uninstall..."
            if($Force -eq "true")
            {
                $urls | ForEach-Object { Disable-SPFeature $installedFeature -confirm:$false -url $_ -force -ErrorAction SilentlyContinue }
                $urls | ForEach-Object { Uninstall-SPFeature $installedFeature -confirm:$false -force -ErrorAction SilentlyContinue }
            } else
            {
                $urls | ForEach-Object { Disable-SPFeature $installedFeature -confirm:$false -url $_ -ErrorAction SilentlyContinue }
                $urls | ForEach-Object { Uninstall-SPFeature $installedFeature -confirm:$false -ErrorAction SilentlyContinue }
            }
        } else 
        {
            Write-Output "       Feature Id $($Id) is not installed nor enabled"
        }
    } else
    {
        Write-Output "       No defined Urls where this Feature is installed. Skipping."
    }
}

function DoInstall-SPSolution
{
    param(
    [string]$SolutionPath,
    [object[]]$WebApplications
    )

    Write-Output "   Going to install the following .wsp file: $($SolutionPath)"
    $solution = Add-SPSolution -LiteralPath $(Get-Childitem $SolutionPath)
    while($solution.JobExists)
    {
        Write-Output "   Deployment in progress..."
        Start-Sleep 5
    }

    if($solution -ne $null)
    {
        Write-Output "   Installation successful. Going to activate feature(s)"
    } else
    {
        Write-Output "   Installation not successful. Unable to add solution. Skipping it"
        return
    }

    foreach($webApp in $WebApplications)
    {
        Write-Output "   WebApp Url: $($webApp.Url), Force: $($webApp.Force), Features: $($features.Count)"
        Write-Output "   Install solution $($solution.Name) to webapplication $($webApp.Url)"
        if($webApp.Force -eq "true")
        {
            Install-SPSolution $solution -GACDeployment -WebApplication $webApp.Url -Force -ErrorAction SilentlyContinue
        }
        else
        {
            Install-SPSolution $solution -GACDeployment -WebApplication $webApp.Url -ErrorAction SilentlyContinue
        }

        $features = $webApp.Features.SelectNodes('Feature')
        DoEnable-SPFeatures -webAppUrl $webApp.Url -force $webApp.Force -features $features
    }
}

function DoEnable-SPFeatures
{
    param(
        [string]$webAppUrl,
        [string]$force,
        [object[]]$features
    )

    foreach($feature in $features)
    {
        $urls = $feature.SelectNodes('Url')
        Write-Output "     WebApp Url: $($webAppUrl), Feature Id: $($feature.Id), Feature Force: $($feature.Force), Urls: $($urls.Count)"
    }
}