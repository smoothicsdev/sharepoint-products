﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="6175e931-b497-4e7a-989e-1827f28de984" defaultResourceFile="Smoothics.FileUploadRestrictions" description="$Resources:FeatureDescription;" featureId="6175e931-b497-4e7a-989e-1827f28de984" imageUrl="Smoothics.FileUploadRestrictions/logo.png" imageAltText="Smoothics Logo" solutionId="00000000-0000-0000-0000-000000000000" title="$Resources:FeatureTitle;" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="76a8af23-4e14-4681-8624-ab1e0ddfb291" />
    <projectItemReference itemId="1d54f12f-cb87-48e4-92cc-755769a4a6c6" />
    <projectItemReference itemId="e6bbc47c-e957-42b7-afa5-7396c987e9da" />
    <projectItemReference itemId="fbf7c872-26b1-4647-b479-a1cef0b7e692" />
  </projectItems>
</feature>