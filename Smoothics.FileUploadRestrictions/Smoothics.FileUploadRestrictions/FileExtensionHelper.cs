﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smoothics.FileUploadRestrictions
{
    /// <summary>
    /// Pre-defined extensions
    /// </summary>
    public class FileExtensionHelper
    {
        public static char[] Delimiters = new char[] { ',', ' ' };

        public static Dictionary<string, string> PredefinedExtensions = new Dictionary<string, string>()
        {
            { "Word", ".docx,.doc" },
            { "Word Templates", ".dotx,.dot" },
            { "Excel", ".xlsx,.xls" },
            { "Powerpoint", ".pptx,.ppt" },
            { "PDF", ".pdf" },
            { "Text", ".txt" },
            { "Rich Text Format", ".rtf" },
            { "JPG", ".jpg,.jpeg" },
            { "Portable Network Graphic", ".png" },
            { "Tagged Image File Format", ".tif,.tiff" },
            { "Bitmap", ".bmp" },
            { "Scalable Vector Graphics", ".svg" },
            { "Graphics Interchange Format", ".gif" }
        };

        public static string GetUINameByExtension(string extension)
        {
            var uiName = string.Empty;
            foreach (var item in PredefinedExtensions)
            {
                string[] extArray = item.Value.Split(Delimiters, StringSplitOptions.RemoveEmptyEntries);
                if(Array.Exists(extArray, elem => elem.Equals(extension)))
                {
                    uiName = item.Key;
                    break;
                }
            }
            return uiName;
        }

        public static string GetAggregatedExtension(string extension)
        {
            var aggregated = string.Empty;
            foreach (var item in PredefinedExtensions)
            {
                string[] extArray = item.Value.Split(Delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (Array.Exists(extArray, elem => elem.Equals(extension)))
                {
                    aggregated = item.Value;
                    break;
                }
            }
            return aggregated;
        }
    }
}
