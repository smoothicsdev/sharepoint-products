﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.WebControls;
using System.Text;
using System.Resources;
using System.Reflection;
using System.Web;
using System.Globalization;
using System.Collections.Generic;
using Microsoft.SharePoint.Utilities;
using System.Linq;

namespace Smoothics.FileUploadRestrictions.Layouts.Smoothics.FileUploadRestrictions
{
    public partial class FileUploadRestrictionSettings : LayoutsPageBase
    {
        public const string VIEWSTATE_FEATURE_IS_ACTIVATED = "featureIsActivated";

        protected override void InitializeCulture()
        {
            base.InitializeCulture();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                InitFeatureActivation();
                InitCheckboxList();
                InitFileSizeValue();
                SetAllowedFileTypes();
                SetButtonActivationText();
            }
        }

        private void InitFeatureActivation()
        {
            var receiver = SPContext.Current.List.EventReceivers.OfType<SPEventReceiverDefinition>()
                                .Where(elem => elem.Class == typeof(FileTypeEventReceiver).FullName && elem.Type == SPEventReceiverType.ItemAdding)
                                .SingleOrDefault();

            var featureIsActivated = receiver != null;
            ViewState[VIEWSTATE_FEATURE_IS_ACTIVATED] = featureIsActivated;
            SetStateForControls(featureIsActivated);
        }

        private void SetButtonActivationText()
        {
            var featureIsActivated = (bool)ViewState[VIEWSTATE_FEATURE_IS_ACTIVATED];
            ButtonFeatureActivation.Text = featureIsActivated ? App_GlobalResources.Smoothics_FileUploadRestrictionSettings.ButtonActivated :
                                                                App_GlobalResources.Smoothics_FileUploadRestrictionSettings.ButtonDeactivated;
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid)
                return;

            var featureIsActivated = (bool) ViewState[VIEWSTATE_FEATURE_IS_ACTIVATED];

            HandleEventhandler(featureIsActivated);
            if (featureIsActivated)
            {
                HandleFileExtension();
                HandleFileSizeRestriction();
            }

            SPUtility.Redirect(SPContext.Current.Web.Url, SPRedirectFlags.UseSource, HttpContext.Current);
        }

        private void HandleEventhandler(bool doAttach)
        {
            var receiver = SPContext.Current.List.EventReceivers.OfType<SPEventReceiverDefinition>()
                            .Where(elem => elem.Class == typeof(FileTypeEventReceiver).FullName && elem.Type == SPEventReceiverType.ItemAdding)
                            .SingleOrDefault();

            if (doAttach)
            {
                if (receiver == null)
                    SPContext.Current.List.EventReceivers.Add(SPEventReceiverType.ItemAdding, Assembly.GetExecutingAssembly().FullName, typeof(FileTypeEventReceiver).FullName);

            }
            else
            {
                if (receiver != null)
                    receiver.Delete();
            }
        }

        private void InitFileSizeValue()
        {
            if (SPContext.Current.List.RootFolder.Properties.ContainsKey(FileTypeEventReceiver.PROPERTYNAME_FILESIZE_RESTRICTION))
            {
                TextBoxFileSizeRestriction.Text = SPContext.Current.List.RootFolder.Properties[FileTypeEventReceiver.PROPERTYNAME_FILESIZE_RESTRICTION].ToString();
            }
        }

        private void HandleFileSizeRestriction()
        {
            if (string.IsNullOrWhiteSpace(TextBoxFileSizeRestriction.Text))
            {
                SPContext.Current.List.RootFolder.Properties.Remove(FileTypeEventReceiver.PROPERTYNAME_FILESIZE_RESTRICTION);
                SPContext.Current.List.RootFolder.Update();
                return;
            }

            double fileSize = 0;
            bool valid = double.TryParse(TextBoxFileSizeRestriction.Text, out fileSize);
            if (valid)
            {
                if (!SPContext.Current.List.RootFolder.Properties.ContainsKey(FileTypeEventReceiver.PROPERTYNAME_FILESIZE_RESTRICTION))
                    SPContext.Current.List.RootFolder.AddProperty(FileTypeEventReceiver.PROPERTYNAME_FILESIZE_RESTRICTION, TextBoxFileSizeRestriction.Text.Trim()); //passing it as string, since only int, string and byte is allowed in SP
                else
                    SPContext.Current.List.RootFolder.SetProperty(FileTypeEventReceiver.PROPERTYNAME_FILESIZE_RESTRICTION, TextBoxFileSizeRestriction.Text.Trim());
                SPContext.Current.List.RootFolder.Update();
            }
            else
            {
                // TODO: error message?
            }
        }

        private void HandleFileExtension()
        {
            var knownExtensions = CheckListFileTypes.Items
                                    .Cast<ListItem>()
                                    .Where(item => item.Selected)
                                    .Select(item => item.Value);

            var knownExtensionsStr = string.Join(",", knownExtensions); //aggregate all known 
                                                                        //extensions since one listitem 
                                                                        //can contain several extensions 
                                                                        //(like .docx,.doc,.dotx,.dot for 
                                                                        //Word and xls, .xlsx for Excel)

            // assure that an extension is only 1x in the list
            var extensions = new HashSet<string>(knownExtensionsStr.Split(FileExtensionHelper.Delimiters, StringSplitOptions.RemoveEmptyEntries));
            var otherExtensions = TextBoxOthers.Text
                                        .ToLowerInvariant()
                                        .Trim()
                                        .Split(FileExtensionHelper.Delimiters, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < otherExtensions.Length; i++)
            {
                extensions.Add(otherExtensions[i]);
            }

            var extensionStr = String.Join(",", extensions.ToArray());
            if (!SPContext.Current.List.RootFolder.Properties.ContainsKey(FileTypeEventReceiver.PROPERTYNAME_FILETYPES))
                SPContext.Current.List.RootFolder.AddProperty(FileTypeEventReceiver.PROPERTYNAME_FILETYPES, extensionStr);
            else
                SPContext.Current.List.RootFolder.SetProperty(FileTypeEventReceiver.PROPERTYNAME_FILETYPES, extensionStr);
            SPContext.Current.List.RootFolder.Update();
        }

        private void InitCheckboxList()
        {
            foreach (var item in FileExtensionHelper.PredefinedExtensions)
            {
                var newItem = new ListItem($"{item.Key} ({item.Value})", item.Value);
                CheckListFileTypes.Items.Add(newItem);
            }
        }

        private void SetAllowedFileTypes()
        {
            if (SPContext.Current.List.RootFolder.Properties.ContainsKey(FileTypeEventReceiver.PROPERTYNAME_FILETYPES))
            {
                var allowedExtensions = ((string)SPContext.Current.List.RootFolder.Properties[FileTypeEventReceiver.PROPERTYNAME_FILETYPES]).Split(FileExtensionHelper.Delimiters, StringSplitOptions.RemoveEmptyEntries);

                HashSet<string> otherExtensions = new HashSet<string>();
                var itemList = CheckListFileTypes.Items.Cast<ListItem>();
                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    var aggregated = FileExtensionHelper.GetAggregatedExtension(allowedExtensions[i]);
                    var listItem = itemList
                                        .Where(item => item.Value.Equals(aggregated))
                                        .SingleOrDefault<ListItem>();
                    if (listItem == null)
                        otherExtensions.Add(allowedExtensions[i]);
                    else
                        listItem.Selected = true;
                }

                TextBoxOthers.Text = string.Join(", ", otherExtensions.ToArray());
            }
        }

        protected void ButtonFeatureActivation_Click(object sender, EventArgs e)
        {

            // toggle state

            bool featureIsActivated = (bool)ViewState[VIEWSTATE_FEATURE_IS_ACTIVATED];
            ViewState[VIEWSTATE_FEATURE_IS_ACTIVATED] = !featureIsActivated;
            SetStateForControls(!featureIsActivated);
            SetButtonActivationText();
        }

        private void SetStateForControls(bool featureIsActivated)
        {
            CheckListFileTypes.Enabled = featureIsActivated;
            TextBoxOthers.Enabled = featureIsActivated;
            TextBoxFileSizeRestriction.Enabled = featureIsActivated;
            RegularExpressionValidatorFileSize.Enabled = featureIsActivated;
        }
    }
}
