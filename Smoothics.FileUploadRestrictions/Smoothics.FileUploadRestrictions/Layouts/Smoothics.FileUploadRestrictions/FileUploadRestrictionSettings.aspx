﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileUploadRestrictionSettings.aspx.cs" Inherits="Smoothics.FileUploadRestrictions.Layouts.Smoothics.FileUploadRestrictions.FileUploadRestrictionSettings" DynamicMasterPageFile="~masterurl/default.master" %>

<%@ Register TagPrefix="spuc" TagName="ButtonSection" Src="~/_controltemplates/15/ButtonSection.ascx" %>
<%@ Register TagPrefix="spuc" TagName="InputFormSection" Src="~/_controltemplates/15/InputFormSection.ascx" %>
<%@ Register TagPrefix="spuc" TagName="InputFormControl" Src="~/_controltemplates/15/InputFormControl.ascx" %>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <table border="0" cellspacing="0" cellpadding="0" width="100%">

        <spuc:InputFormSection runat="server" Title="<% $Resources:Smoothics.FileUploadRestrictionSettings,TitleActivateFeature %>">
            <template_description>
                <asp:Literal ID="Literal4" runat="server" text="<% $Resources:Smoothics.FileUploadRestrictionSettings,TitleActivateFeatureDescription %>"/>
            </template_description>
            <Template_InputFormControls>
                <spuc:InputFormControl runat="server">
                    <Template_Control>
                        <asp:Button ID="ButtonFeatureActivation" runat="server" OnClick="ButtonFeatureActivation_Click"></asp:Button>
                    </Template_Control>
                </spuc:InputFormControl>
            </Template_InputFormControls>
        </spuc:InputFormSection>

        <spuc:InputFormSection runat="server" Title="<% $Resources:Smoothics.FileUploadRestrictionSettings,TitleCheckboxSelection %>">
            <template_description>
                <asp:Literal ID="Literal2" runat="server" text="<% $Resources:Smoothics.FileUploadRestrictionSettings,Description %>"/>
            </template_description>
            <Template_InputFormControls>
                <spuc:InputFormControl runat="server">
                    <Template_Control>
                        <div><asp:CheckBoxList ID="CheckListFileTypes" runat="server"></asp:CheckBoxList></div>
                        <div style="float: left;">
                            <asp:Label ID="LabelOthers" runat="server" Text="<% $Resources:Smoothics.FileUploadRestrictionSettings,FileTypeOthersUI %>"></asp:Label>
                            <asp:TextBox ID="TextBoxOthers" runat="server"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExressionValidatorTextboxOthers" runat="server"
                                Display="Dynamic"
                                ErrorMessage="<% $Resources:Smoothics.FileUploadRestrictionSettings,TextboxFileInputError %>"
                                ValidationExpression="^[\s\t]*\.[A-Za-z0-9]+[\s\t]*(,[\s\t]*\.[A-Za-z0-9]+[\s\t]*)*$"
                                ControlToValidate="TextBoxOthers"
                                ForeColor="Red">
                            </asp:RegularExpressionValidator>
                        </div>
                        <div style="clear: both;"></div>
                    </Template_Control>
                </spuc:InputFormControl>
            </Template_InputFormControls>
        </spuc:InputFormSection>

        <spuc:InputFormSection runat="server" Title="<% $Resources:Smoothics.FileUploadRestrictionSettings,TitleFileSize %>">
            <template_description>
                <asp:Literal ID="Literal3" runat="server" text="<% $Resources:Smoothics.FileUploadRestrictionSettings,FileSizeRestrictionUI %>"/>
            </template_description>
            <Template_InputFormControls>
                <spuc:InputFormControl runat="server">
                    <Template_Control>
                        <asp:TextBox ID="TextBoxFileSizeRestriction" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorFileSize" runat="server"
                            Display="Dynamic"
                            ErrorMessage="<% $Resources:Smoothics.FileUploadRestrictionSettings,TextboxFileSizeError %>"
                            ValidationExpression="^[\s\t]*\d+((,|\.)\d*)?[\s\t]*$"
                            ControlToValidate="TextBoxFileSizeRestriction"
                            ForeColor="Red">
                        </asp:RegularExpressionValidator>
                    </Template_Control>
                </spuc:InputFormControl>
            </Template_InputFormControls>
        </spuc:InputFormSection>

        <spuc:ButtonSection runat="server">
            <template_buttons>
                <asp:Button id="btn_Save" runat="server" UseSubmitBehavior="false" OnClick="Submit_Click" Text="<%$Resources:wss,multipages_okbutton_text%>" class="ms-ButtonHeightWidth"/>
            </template_buttons>
        </spuc:ButtonSection>
    </table>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    <asp:Literal ID="Literal1" runat="server" Text="<% $Resources:Smoothics.FileUploadRestrictionSettings,TitleTab %>" />
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    <asp:Literal ID="Literal5" runat="server" Text="<% $Resources:Smoothics.FileUploadRestrictionSettings,TitleArea %>" />
</asp:Content>
