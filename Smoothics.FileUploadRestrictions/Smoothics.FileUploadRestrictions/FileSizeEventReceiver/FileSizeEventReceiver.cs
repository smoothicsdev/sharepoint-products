﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;
using System.Resources;
using System.Reflection;

namespace Smoothics.FileUploadRestrictions
{
    /// <summary>
    /// List Item Events
    /// </summary>
    public class FileSizeEventReceiver : SPItemEventReceiver
    {
        public const string PROPERTYNAME_FILESIZE = "_smoothics_restricted_filesize";

        public FileSizeEventReceiver() : base()
        {
        }

        /// <summary>
        /// An item is being added.
        /// </summary>
        public override void ItemAdding(SPItemEventProperties properties)
        {
            if (properties.List.RootFolder.Properties.ContainsKey(PROPERTYNAME_FILESIZE))
            {
                //TODO
            }
        }
    }
}