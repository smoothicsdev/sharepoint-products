﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;
using System.IO;
using System.Linq;
using System.Text;
using System.Resources;
using System.Reflection;
using System.Collections.Generic;

namespace Smoothics.FileUploadRestrictions
{
    /// <summary>
    /// List Item Events
    /// </summary>
    public class FileTypeEventReceiver : SPItemEventReceiver
    {
        public const string PROPERTYNAME_FILETYPES = "_smoothics_filetypes_allowed";
        public const string PROPERTYNAME_FILESIZE_RESTRICTION = "_smoothics_filesize_restricted";

        public FileTypeEventReceiver() : base()
        {
        }

        /// <summary>
        /// An item is being added.
        /// </summary>
        public override void ItemAdding(SPItemEventProperties properties)
        {

            // FILE EXTENSION

            if (properties.List.RootFolder.Properties.ContainsKey(PROPERTYNAME_FILETYPES))
            {
                var allowedExtensions = ((string)properties.List.RootFolder.Properties[PROPERTYNAME_FILETYPES]).Split(FileExtensionHelper.Delimiters, StringSplitOptions.RemoveEmptyEntries);
                var newFileExtension = Path.GetExtension(properties.AfterUrl).ToLowerInvariant();
                var isAllowed = allowedExtensions.Where(extension => extension.Equals(newFileExtension)).Count() > 0;
                if (!isAllowed)
                {
                    properties.ErrorMessage = CreateErrorMessageBy(allowedExtensions);
                    properties.Status = SPEventReceiverStatus.CancelWithError;
                    return;
                }
            }

            // FILE SIZE RESTRICTION

            if (properties.List.RootFolder.Properties.ContainsKey(PROPERTYNAME_FILESIZE_RESTRICTION))
            {
                int newFileSizeInBytes = (int)properties.AfterProperties["vti_filesize"];
                double allowedFileSizeInMB = 0.0;
                var allowedFileSizeStr = (string)properties.List.RootFolder.Properties[PROPERTYNAME_FILESIZE_RESTRICTION];
                var allowedSizeIsValid = double.TryParse(allowedFileSizeStr, out allowedFileSizeInMB);
                if(!allowedSizeIsValid)
                {
                    // TODO: (internal) error message?
                    return;
                }

                var inBytes = (int)(allowedFileSizeInMB * 1000000);
                if (newFileSizeInBytes > inBytes)
                {
                    properties.ErrorMessage = CreateFileSizeErrorMessage(allowedFileSizeStr);
                    properties.Status = SPEventReceiverStatus.CancelWithError;
                    return;
                }
            }
        }

        private string CreateFileSizeErrorMessage(string fileSize)
        {
            return string.Format(App_GlobalResources.Smoothics_FileUploadRestrictionSettings.TextboxFileSizeReceiverError, fileSize);
        }

        private string CreateErrorMessageBy(string[] allowedExtensions)
        {
            if (allowedExtensions.Length == 0)
                return string.Empty;

            var knownExtensions = new HashSet<string>();
            var otherExtensions = new HashSet<string>();
            for (int i = 0; i < allowedExtensions.Length; i++)
            {
                var uiName = FileExtensionHelper.GetUINameByExtension(allowedExtensions[i]);
                if (!string.IsNullOrWhiteSpace(uiName))
                    knownExtensions.Add(uiName);
                else
                    otherExtensions.Add(allowedExtensions[i]);
            }

            string text = $"{App_GlobalResources.Smoothics_FileUploadRestrictionSettings.FileTypeNotAllowedUI} ";
            if (knownExtensions.Count > 0)
            {
                text += string.Format(App_GlobalResources.Smoothics_FileUploadRestrictionSettings.FileTypeNotAllowedKnownTypesUI, string.Join(", ", knownExtensions.ToArray()));
            }

            if (otherExtensions.Count > 0)
            {
                if (otherExtensions.Count == 1)
                    text += string.Format(App_GlobalResources.Smoothics_FileUploadRestrictionSettings.FileTypeNotAllowedOtherTypesUI, otherExtensions.First().ToLowerInvariant());
                else
                    text += string.Format(App_GlobalResources.Smoothics_FileUploadRestrictionSettings.FileTypeNotAllowedOtherTypesUIPlural, string.Join(", ", otherExtensions.ToArray()));
            }

            return text.Trim();
        }
    }
}