﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Smoothics.ExcelImport.ControlTemplates.Smoothics.ExcelImport;
using System.Xml.Serialization;
using System.Text;
using System.IO;
using System.Xml;

namespace Smoothics.ExcelImportTest
{
    /// <summary>
    /// Sorry, not really unittest like we learned :-) - only integration tests for developing and for testing some code.
    /// </summary>
    [TestClass]
    public class SPListExcelImportConfigurationTest
    {
        [TestMethod]
        public void CanXmlSerializeAndDeserialize()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SPListExcelImportConfiguration));
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            // Serialize
            SPListExcelImportConfiguration spListConfig = CreateNewSPListExcelConfiguration();
            serializer.Serialize(sw, spListConfig);
            string xmlResult = sw.GetStringBuilder().ToString();

            // Deserialize back to object
            var deSerializer = new XmlSerializer(typeof(SPListExcelImportConfiguration));
            SPListExcelImportConfiguration result;
            using (TextReader reader = new StringReader(xmlResult))
            {
                result = (SPListExcelImportConfiguration)deSerializer.Deserialize(reader);
            }

            Assert.AreEqual("123456789", result.SPListId);
            Assert.AreEqual("Config 1", result.MappingConfigurations[0].Name);
            Assert.IsTrue(result.MappingConfigurations[0].ClearDataBeforeImport);
            Assert.AreEqual("myfile1.xls", result.MappingConfigurations[0].ExcelFileName);
            Assert.AreEqual(2, result.MappingConfigurations[0].Sheets.Count);
            Assert.AreEqual(2, result.MappingConfigurations[0].Sheets[0].TargetListMappings.Count);
            Assert.AreEqual(2, result.MappingConfigurations[0].Sheets[0].TargetListMappings[0].MappingValues.Count);

            // XPath
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlResult);

            Assert.IsNotNull(xmlResult);
            Assert.IsNotNull(doc.SelectSingleNode("/SPListExcelImportConfiguration"));
            Assert.IsNotNull(doc.SelectSingleNode("/SPListExcelImportConfiguration/SPListId"));
            Assert.AreEqual(2, doc.SelectNodes("/SPListExcelImportConfiguration/MappingConfigurations/MappingConfiguration").Count);
        }

        private static SPListExcelImportConfiguration CreateNewSPListExcelConfiguration()
        {
            var spListConfig = new SPListExcelImportConfiguration() { SPListId = "123456789" };

            var mappingConfig1 = new MappingConfiguration() { Name = "Config 1", ClearDataBeforeImport = true, ExcelFileName = "myfile1.xls" };
            var mappingConfig2 = new MappingConfiguration() { Name = "Config 2", ClearDataBeforeImport = false, ExcelFileName = "myfile2.xls" };

            var sheetConfig1 = new SheetConfiguration() { Name = "MySheet 1" };
            var sheetConfig2 = new SheetConfiguration() { Name = "MySheet 2" };
            var sheetConfig3 = new SheetConfiguration() { Name = "MySheet 3" };

            var targetMapping1 = new TargetSPListMapping() { SharePointSite = "", SharePointListName = "Target List 1", SharePointListId = "1111-1111", ContentTypeId = "1111", ContentTypeName = "Content Type 1" };
            var targetMapping2 = new TargetSPListMapping() { SharePointSite = "http://SP2016/sites/st", SharePointListName = "Target List 2", SharePointListId = "2222-2222", ContentTypeId = "2222", ContentTypeName = "Content Type 2" };
            var targetMapping3 = new TargetSPListMapping() { SharePointSite = "", SharePointListName = "Target List 3", SharePointListId = "3333-3333", ContentTypeId = "3333", ContentTypeName = "Content Type 3" };
            var targetMapping4 = new TargetSPListMapping() { SharePointSite = "", SharePointListName = "Target List 4", SharePointListId = "4444-4444", ContentTypeId = "4444", ContentTypeName = "Content Type 4" };

            var mappingValue1 = new MappingValue() { ContentTypeName = "Contenttype 1", ContentTypeId = "111", ExcelColumn = "A" };
            var mappingValue2 = new MappingValue() { ContentTypeName = "Contenttype 2", ContentTypeId = "222", ExcelColumn = "B" };
            var mappingValue3 = new MappingValue() { ContentTypeName = "Contenttype 3", ContentTypeId = "333", ExcelColumn = "C" };
            var mappingValue4 = new MappingValue() { ContentTypeName = "Contenttype 4", ContentTypeId = "444", ExcelColumn = "D" };
            var mappingValue5 = new MappingValue() { ContentTypeName = "Contenttype 5", ContentTypeId = "555", ExcelColumn = "E" };
            var mappingValue6 = new MappingValue() { ContentTypeName = "Contenttype 6", ContentTypeId = "666", ExcelColumn = "F" };

            spListConfig.MappingConfigurations.Add(mappingConfig1);
            spListConfig.MappingConfigurations.Add(mappingConfig2);

            mappingConfig1.Sheets.Add(sheetConfig1);
            mappingConfig1.Sheets.Add(sheetConfig2);
            mappingConfig2.Sheets.Add(sheetConfig3);

            sheetConfig1.TargetListMappings.Add(targetMapping1);
            sheetConfig1.TargetListMappings.Add(targetMapping2);
            sheetConfig2.TargetListMappings.Add(targetMapping3);
            sheetConfig3.TargetListMappings.Add(targetMapping4);

            targetMapping1.MappingValues.Add(mappingValue1);
            targetMapping1.MappingValues.Add(mappingValue2);
            targetMapping2.MappingValues.Add(mappingValue3);
            targetMapping3.MappingValues.Add(mappingValue4);
            targetMapping4.MappingValues.Add(mappingValue5);
            targetMapping4.MappingValues.Add(mappingValue6);

            return spListConfig;
        }
    }
}
