﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Smoothics.ExcelImport;
using System.IO;
using Microsoft.SharePoint;

namespace Smoothics.ExcelImportTest
{
    /// <summary>
    /// Sorry, not really unittest like we learned :-) - only integration tests for developing and for testing some code.
    /// Excel testfile 'TestBook1.xlsx' is prepared (see folder TestData in this project).
    /// </summary>
    [TestClass]
    public class ExcelHelperTest
    {
        [TestMethod]
        public void CanCreateByFilePath()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");

            try
            {
                using (var excelHelper = new ExcelHelper(testpath))
                {
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void CanCreateByStream()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            try
            {

                using (var excelHelper = new ExcelHelper(filestream))
                {
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void CanGetColumnNameByIndex()
        {
            var columnName = ExcelHelper.GetColumnNameByIndex(27);

            Assert.AreEqual("AA", columnName);
        }

        [TestMethod]
        public void CanGetIndexByColumnName()
        {
            int idx = ExcelHelper.GetIndexByColumnName("AA");

            Assert.AreEqual(27, idx);
        }

        [TestMethod]
        public void CanGetMaxUsedColumnBySheetPosition()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            using (var excelHelper = new ExcelHelper(filestream))
            {
                try
                {
                    var maxColumn = excelHelper.GetMaxUsedColumns(1);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
            }
        }

        [TestMethod]
        public void CanGetMaxUsedColumnBySheetName()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            using (var excelHelper = new ExcelHelper(filestream))
            {
                try
                {
                    var maxColumn = excelHelper.GetMaxUsedColumns("MySheet");
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
            }
        }

        [TestMethod]
        public void CanGetMaxUsedRowsBySheetPosition()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            using (var excelHelper = new ExcelHelper(filestream))
            {
                try
                {
                    var maxColumn = excelHelper.GetMaxUsedRows(1);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
            }
        }

        [TestMethod]
        public void CanGetMaxUsedRowsBySheetName()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            using (var excelHelper = new ExcelHelper(filestream))
            {
                try
                {
                    var maxColumn = excelHelper.GetMaxUsedRows("MySheet");
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
            }
        }

        [TestMethod]
        public void CanGetCell_BySheetNameExcelCell()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            using (var excelHelper = new ExcelHelper(filestream))
            {
                try
                {
                    var cellValue = excelHelper.GetCellValue("MySheet", "A1");
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
            }
        }

        [TestMethod]
        public void CanGetCell_BySheetNameRowColumn()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            using (var excelHelper = new ExcelHelper(filestream))
            {
                try
                {
                    var cellValue = excelHelper.GetCellValue("MySheet", 1, 1);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
            }
        }

        [TestMethod]
        public void CanGetCell_BySheetPositionExcelCell()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            using (var excelHelper = new ExcelHelper(filestream))
            {
                try
                {
                    var cellValue = excelHelper.GetCellValue(1, "A1");
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
            }
        }

        [TestMethod]
        public void CanGetCell_BySheetPositionRowColumn()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            using (var excelHelper = new ExcelHelper(filestream))
            {
                try
                {
                    var cellValue = excelHelper.GetCellValue(1, 1, 1);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
            }
        }

        [TestMethod]
        public void CanGetCellValue_AsBoolean()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            using (var excelHelper = new ExcelHelper(filestream))
            {
                try
                {
                    var cellValueBool1 = excelHelper.GetCellValueAsBoolean("Datatype Test", "I2");
                    var cellValueBool2 = excelHelper.GetCellValueAsBoolean("Datatype Test", "J2");
                    var cellValueBool3 = excelHelper.GetCellValueAsBoolean("Datatype Test", "K2");
                    var cellValueBool4 = excelHelper.GetCellValueAsBoolean(2, 2, 9);
                    var cellValueBool5 = excelHelper.GetCellValueAsBoolean(2, 2, 10);
                    var cellValueBool6 = excelHelper.GetCellValueAsBoolean(2, 2, 11);
                    var cellValueBool7 = excelHelper.GetCellValueAsBoolean("Datatype Test", "A2");
                    var cellValueBool8 = excelHelper.GetCellValueAsBoolean(2, 2, 1);

                    Assert.IsTrue(cellValueBool1);
                    Assert.IsTrue(cellValueBool2);
                    Assert.IsTrue(cellValueBool3);
                    Assert.IsTrue(cellValueBool4);
                    Assert.IsTrue(cellValueBool5);
                    Assert.IsTrue(cellValueBool6);
                    Assert.IsFalse(cellValueBool7);
                    Assert.IsFalse(cellValueBool8);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
            }
        }

        [TestMethod]
        public void CanGetCellValue_AsNumber()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            using (var excelHelper = new ExcelHelper(filestream))
            {
                try
                {
                    var cellValueNumber1 = excelHelper.GetCellValueAsNumber("Datatype Test", "B2");
                    var cellValueNumber2 = excelHelper.GetCellValueAsNumber("Datatype Test", "E2");
                    var cellValueNumber3 = excelHelper.GetCellValueAsNumber("Datatype Test", "F2");
                    var cellValueNumber4 = excelHelper.GetCellValueAsNumber("Datatype Test", "G2");

                    var cellValueNumber5 = excelHelper.GetCellValueAsNumber(2, 2, 2);
                    var cellValueNumber6 = excelHelper.GetCellValueAsNumber(2, 2, 5);
                    var cellValueNumber7 = excelHelper.GetCellValueAsNumber(2, 2, 6);
                    var cellValueNumber8 = excelHelper.GetCellValueAsNumber(2, 2, 7);

                    var cellValueNumber9 = excelHelper.GetCellValueAsNumber("Datatype Test", "A2"); // note: test value is not a number
                    var cellValueNumber10 = excelHelper.GetCellValueAsNumber(2, 2, 1);              // note: test value is not a number

                    Assert.AreEqual(123.0f, cellValueNumber1);
                    Assert.AreEqual(123.0f, cellValueNumber2);
                    Assert.AreEqual(123.0f, cellValueNumber3);
                    Assert.AreEqual(246.0f, cellValueNumber4);
                    Assert.AreEqual(123.0f, cellValueNumber5);
                    Assert.AreEqual(123.0f, cellValueNumber6);
                    Assert.AreEqual(123.0f, cellValueNumber7);
                    Assert.AreEqual(246.0f, cellValueNumber8);
                    Assert.AreEqual(0.0f, cellValueNumber9);
                    Assert.AreEqual(0.0f, cellValueNumber10);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
            }
        }

        [TestMethod]
        public void CanGetCellValue_AsDateTime()
        {
            var testpath = Path.Combine("TestData", "TestBook1.xlsx");
            var filestream = File.OpenRead(testpath);

            using (var excelHelper = new ExcelHelper(filestream))
            {
                try
                {
                    var cellValueDateTime1 = excelHelper.GetCellValueAsDateTime("Datatype Test", "H2");
                    var cellValueDateTime2 = excelHelper.GetCellValueAsDateTime("Datatype Test", "A2"); // note: test value is not a datetime

                    Assert.AreEqual(new DateTime(2017,11,27).ToShortDateString(), cellValueDateTime1.ToShortDateString());
                    Assert.AreEqual(DateTime.MinValue.ToShortDateString(), cellValueDateTime2.ToShortDateString());
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
            }
        }
    }
}
