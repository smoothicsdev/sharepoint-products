﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExcelImportSettings.aspx.cs" Inherits="Smoothics.ExcelImport.Layouts.Smoothics.ExcelImport.ExcelImportSettings" DynamicMasterPageFile="~masterurl/default.master" %>
<%@ Register TagPrefix="smoothics" TagName="ExcelImportSettingsControl" Src="~/_controltemplates/15/Smoothics.ExcelImport/ExcelImportSettingsControl.ascx" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">

   <smoothics:ExcelImportSettingsControl ID="ExcelImportSettingsControl1" runat="server"></smoothics:ExcelImportSettingsControl>

</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    <asp:Literal ID="Literal1" runat="server" Text="<% $Resources:Smoothics.ExcelImportSettings,TitleTab %>" />
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    <asp:Literal ID="Literal5" runat="server" Text="<% $Resources:Smoothics.ExcelImportSettings,TitleArea %>" />
</asp:Content>
