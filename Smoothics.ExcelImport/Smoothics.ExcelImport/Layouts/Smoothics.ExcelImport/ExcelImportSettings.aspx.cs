﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Utilities;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Smoothics.ExcelImport.Layouts.Smoothics.ExcelImport
{
    public partial class ExcelImportSettings : LayoutsPageBase
    {
        public const string VIEWSTATE_FEATURE_IS_ACTIVATED = "featureIsActivated";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
            }
        }



        private void SetButtonActivationText()
        {
            var featureIsActivated = (bool)ViewState[VIEWSTATE_FEATURE_IS_ACTIVATED];

            //ButtonFeatureActivation.Text = featureIsActivated ? App_GlobalResources.Smoothics_ExcelImportSettings.ButtonActivated :
            //                                                    App_GlobalResources.Smoothics_ExcelImportSettings.ButtonDeactivated;
        }


        protected void Submit_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid)
                return;

            var featureIsActivated = (bool)ViewState[VIEWSTATE_FEATURE_IS_ACTIVATED];
            DoAttachEventhandler(featureIsActivated);
            if (featureIsActivated)
            {
                //TODO
            }
            else
            {
                SPContext.Current.List.RootFolder.Properties.Remove(ExcelFileEventReceiver.SP_PROPERTYNAME_EXCELIMPORTMAPPINGS);
                SPContext.Current.List.RootFolder.Update();
            }

            SPUtility.Redirect(SPContext.Current.Web.Url, SPRedirectFlags.UseSource, HttpContext.Current);
        }

        private void DoAttachEventhandler(bool doAttach)
        {
            var receiver = SPContext.Current.List.EventReceivers.OfType<SPEventReceiverDefinition>()
                            .Where(elem => elem.Class == typeof(ExcelFileEventReceiver).FullName && (elem.Type == SPEventReceiverType.ItemUpdated))
                            .ToList();

            if (doAttach)
            {
                if (receiver == null || receiver.Count == 0)
                {
                    SPContext.Current.List.EventReceivers.Add(SPEventReceiverType.ItemUpdated, Assembly.GetExecutingAssembly().FullName, typeof(ExcelFileEventReceiver).FullName);
                }
            }
            else
            {
                foreach (var item in receiver)
                {
                    item.Delete();
                }
            }
            SPContext.Current.List.Update();
        }

        protected void ButtonFeatureActivation_Click(object sender, EventArgs e)
        {
            // toggle state
            bool featureIsActivated = (bool)ViewState[VIEWSTATE_FEATURE_IS_ACTIVATED];
            ViewState[VIEWSTATE_FEATURE_IS_ACTIVATED] = !featureIsActivated;
            SetButtonActivationText();
        }

    }

}