﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smoothics.ExcelImport.ControlTemplates.Smoothics.ExcelImport
{
    public enum StatusLevel
    {
        /// <summary>
        /// Just an info (or message) for the end user.
        /// </summary>
        Info,
        /// <summary>
        /// An action was successfully executed.
        /// </summary>
        Success,
        /// <summary>
        /// Application warning caused by user (i.e. insufficient input/data). Application
        /// can proceed, but it may cause problems later.
        /// </summary>
        Warning,
        /// <summary>
        /// Application error caused by user (i.e. missing input fields)
        /// </summary>
        Error,
        /// <summary>
        /// System error. User should get a sysadmin for this problem.
        /// </summary>
        Fatal
    }
}
