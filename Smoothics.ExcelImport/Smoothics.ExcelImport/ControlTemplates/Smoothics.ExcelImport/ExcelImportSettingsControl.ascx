﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExcelImportSettingsControl.ascx.cs" Inherits="Smoothics.ExcelImport.ControlTemplates.Smoothics.ExcelImport.ExcelImportSettingsControl" %>

<div class="tab">
    <asp:PlaceHolder ID="PlaceHolderMappingConfigButtons" runat="server"></asp:PlaceHolder>
</div>

<div class="tabcontent">

    <%--MAPPING HEADER--%>
    <div>
        <div>
            <asp:Label ID="Label1" AssociatedControlID="TextBoxMappingConfiguration1" runat="server" Text="Name" CssClass="labels" />
            <asp:TextBox ID="TextBoxMappingConfiguration1" runat="server" />
            <asp:LinkButton ID="LinkButton1" runat="server"></asp:LinkButton>
        </div>
        <div>
            <asp:Label ID="Label2" AssociatedControlID="TextBoxExcelFilename1" runat="server" Text="Dateiname" CssClass="labels" ToolTip="Ein Excel-Import erfolgt nur auf folgende Dateiname" />
            <asp:TextBox ID="TextBoxExcelFilename1" runat="server" />
            <asp:Label ID="Label3" runat="server" Text=".xls (.xlsx)" />
        </div>
        <div style="margin-top: 10px; margin-bottom: 20px;">
            <asp:Label ID="Label4" AssociatedControlID="CheckBoxClearDataBeforeImport1" runat="server" CssClass="labels" />
            <asp:CheckBox ID="CheckBoxClearDataBeforeImport1" runat="server" Text="Lösche vorhandene Listendaten vor dem Import" />
        </div>
    </div>

    <%--SHEET HEADER--%>
    <asp:Panel ID="PanelSheetConfigurations" runat="server">
        <div class="tab">
            <asp:PlaceHolder ID="PlaceHolderSheetConfigButtons" runat="server"></asp:PlaceHolder>
        </div>

        <div class="tabcontent">
            <div>
                <asp:Label ID="Label11" AssociatedControlID="TextBoxSheetName1" runat="server" Text="Name" CssClass="labels" />
                <asp:TextBox ID="TextBoxSheetName1" runat="server" />
            </div>

            <asp:Panel ID="PanelTargetListConfigurations" runat="server">
                <div>
                    <asp:Label ID="Label5" runat="server" Text="Sharepoint Zielliste(n)" ToolTip="Es können die Spalten aus der Exceldatei in mehreren SharePoint Listen gemapped werden." CssClass="header1"></asp:Label>
                </div>
                <div style="margin-top: 5px;">
                    <asp:Label ID="LabelSharepointSite" AssociatedControlID="DropDownListSharePointSiteSelection" runat="server" Text="Site" CssClass="labels"></asp:Label>
                    <asp:DropDownList ID="DropDownListSharePointSiteSelection" runat="server" CssClass="selectbox" AutoPostBack="true" OnSelectedIndexChanged="DropDownListSharePointSiteSelection_SelectedIndexChanged">
                        <asp:ListItem Text="Aktuelle Seite" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Andere Seite" Value="1"></asp:ListItem>
                    </asp:DropDownList><br />
                    <asp:Panel ID="PanelSharePointSiteOtherBox" runat="server" Visible="false">
                        <asp:Label ID="Label6" AssociatedControlID="TextBoxSharePointOtherSite" runat="server" CssClass="labels"></asp:Label>
                        <asp:TextBox ID="TextBoxSharePointOtherSite" runat="server" />
                        <asp:Button ID="ButtonTestAndLoadSharePointSite" runat="server" Text="Testen & Laden" CssClass="defaultButton" UseSubmitBehavior="false" OnClick="ButtonTestAndLoadSharePointSite_Click" />
                        <br />
                        <asp:Label ID="Label8" AssociatedControlID="LabelTestAndLoadResult" runat="server" CssClass="labels"></asp:Label>
                        <asp:Label ID="LabelTestAndLoadResult" runat="server"></asp:Label>
                    </asp:Panel>

                </div>
                <div style="margin-top: 5px;">
                    <asp:Label ID="LabelSharePointLists" AssociatedControlID="DropDownListSharePointLists" runat="server" Text="Verfügbare Listen" CssClass="labels"></asp:Label>
                    <asp:DropDownList ID="DropDownListSharePointLists" runat="server" CssClass="selectbox" AutoPostBack="true" OnSelectedIndexChanged="DropDownListSharePointLists_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div style="margin-top: 5px;">
                    <asp:Label ID="LabelContentType" AssociatedControlID="DropDownListContentType" runat="server" Text="Inhaltstypen" CssClass="labels"></asp:Label>
                    <asp:DropDownList ID="DropDownListContentType" runat="server" CssClass="selectbox">
                    </asp:DropDownList>
                    <asp:Button ID="ButtonAddNewSheetMappingList" runat="server" Text="Hinzufügen" OnClick="ButtonAddNewSheetMappingList_Click" CssClass="defaultButton" UseSubmitBehavior="false" />
                </div>

                <%--TARGET LIST MAPPINGS--%>
                <div style="margin-top: 5px;">
                    <asp:Label ID="Label7" runat="server" Text="Festgelegte SharePoint Zielliste(n)" ToolTip="Auflistung aller Mappings von 'Sheet 1'." CssClass="header1" />
                    <asp:PlaceHolder ID="PlaceHolderAllTargetListMappings" runat="server"></asp:PlaceHolder>
                </div>
            </asp:Panel>

        </div>
    </asp:Panel>

</div>

<div style="margin-top: 15px; margin-bottom: 15px; float: right; display: block;">
    <asp:Button ID="ButtonSaveAll" runat="server" Text="Speichern" UseSubmitBehavior="false" OnClick="ButtonSaveOrCancel_Click" />
    <asp:Button ID="ButtonCancel" runat="server" Text="Abbrechen" UseSubmitBehavior="false" OnClick="ButtonSaveOrCancel_Click" />
</div>
