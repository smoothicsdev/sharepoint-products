﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Smoothics.ExcelImport.ControlTemplates.Smoothics.ExcelImport
{
    [Serializable]
    [XmlRoot(ElementName = "MappingValue")]
    public class MappingValue
    {
        [XmlAttribute]
        public string ContentTypeName { get; set; }
        [XmlAttribute]
        public string ContentTypeId { get; set; }
        [XmlAttribute]
        public string ExcelColumn { get; set; }
    }
}