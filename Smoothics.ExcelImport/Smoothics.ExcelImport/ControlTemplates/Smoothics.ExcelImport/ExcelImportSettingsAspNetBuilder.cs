﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace Smoothics.ExcelImport.ControlTemplates.Smoothics.ExcelImport
{
   /// <summary>
   /// Helper class for generating ASP.NET controls for ExcelImportSettings.
   /// </summary>
   public class ExcelImportSettingsAspNetBuilder
   {
      public Panel CreateTabs(List<string> configTabNames, EventHandler eventHandler)
      {
         // -- SNIPPET --
         //
         //<div class="tab">
         //   <asp:Button ID="Button2" runat="server" Text="Config 1" OnClick="LinkButtonTab_Click" UseSubmitBehavior="false" />
         //   <asp:Button ID="Button1" runat="server" Text="+" OnClick="LinkButtonTab_Click" UseSubmitBehavior="false" />
         //</div>

         Panel panel1 = new Panel();
         panel1.CssClass = "tab";
         foreach (var name in configTabNames)
         {
            var btn = new Button();
            btn.Text = name;
            btn.UseSubmitBehavior = false;
            btn.Click += eventHandler;
            panel1.Controls.Add(btn);
         }
         var buttonAdd = new Button();
         buttonAdd.Text = "+";
         buttonAdd.UseSubmitBehavior = false;
         buttonAdd.Click += eventHandler;
         panel1.Controls.Add(buttonAdd);

         return panel1;
      }

      public Panel CreateHeader(string nameOfMapping, int consecutiveViewNumber)
      {
         // -- SNIPPET --
         //
         //<div>
         //   <div>
         //      <asp:Label ID="Label1" AssociatedControlID="TextBoxMappingConfiguration1" runat="server" Text="Name" CssClass="labels" />
         //      <asp:TextBox ID="TextBoxMappingConfiguration1" runat="server" Text="Config 1" />
         //   </div>
         //   <div>
         //      <asp:Label ID="Label2" AssociatedControlID="TextBoxExcelFilename1" runat="server" Text="Dateiname" CssClass="labels" ToolTip="Ein Excel-Import erfolgt nur auf folgende Dateiname" />
         //      <asp:TextBox ID="TextBoxExcelFilename1" runat="server" />
         //      <asp:Label ID="Label3" runat="server" Text=".xls (.xlsx)" />
         //   </div>
         //   <div style="margin-top: 10px; margin-bottom: 20px;">
         //      <asp:Label ID="Label4" AssociatedControlID="CheckBoxClearDataBeforeImport1" runat="server" CssClass="labels" />
         //      <asp:CheckBox ID="CheckBoxClearDataBeforeImport1" runat="server" Text="Lösche vorhandene Listendaten vor dem Import" />
         //   </div>
         //</div>

         Panel panel1 = new Panel();

         TextBox textbox1 = new TextBox();
         textbox1.ID = $"TextBoxMappingConfiguration{consecutiveViewNumber}";
         textbox1.Text = $"{nameOfMapping}";
         Label label1 = new Label();
         label1.AssociatedControlID = textbox1.ID;
         label1.Text = "Name";
         label1.CssClass = "labels";
         Panel panel2 = new Panel();
         panel2.Controls.Add(label1);
         panel2.Controls.Add(textbox1);

         TextBox textbox2 = new TextBox();
         textbox2.ID = $"TextBoxExcelFilename{consecutiveViewNumber}";
         Label label2 = new Label();
         label2.AssociatedControlID = textbox2.ID;
         label2.CssClass = "labels";
         label2.Text = "Dateiname";
         Label label3 = new Label();
         label3.Text = ".xls (xlsx)";
         Panel panel3 = new Panel();
         panel3.Attributes["style"] = "margin-top: 10px; margin-bottom: 20px;";
         panel3.Controls.Add(label2);
         panel3.Controls.Add(textbox2);
         panel3.Controls.Add(label3);

         CheckBox checkbox1 = new CheckBox();
         checkbox1.ID = $"CheckBoxClearDataBeforeImport{consecutiveViewNumber}";
         checkbox1.Text = $"&nbsp;Lösche vorhandene Listendaten vor dem Excel-Import";
         Label label4 = new Label();
         label4.AssociatedControlID = checkbox1.ID;
         label4.CssClass = "labels";
         Panel panel4 = new Panel();
         panel4.Controls.Add(label4);
         panel4.Controls.Add(checkbox1);

         panel1.Controls.Add(panel2);
         panel1.Controls.Add(panel3);
         panel1.Controls.Add(panel4);

         return panel1;
      }

      public Panel CreateNewTextbox(string name, int consecutiveNumber, string textContent)
      {
         // -- SNIPPET --
         //
         //<div>
         //   <asp:Label ID="Label11" AssociatedControlID="TextBoxSheetName1" runat="server" Text="Name" CssClass="labels" />
         //   <asp:TextBox ID="TextBoxSheetName1" runat="server" Text="Sheet 1" />
         //</div>

         Panel panel1 = new Panel();
         TextBox textbox1 = new TextBox();
         textbox1.ID = $"{name}{consecutiveNumber}";
         textbox1.Text = $"{textContent}";
         Label label1 = new Label();
         label1.AssociatedControlID = textbox1.ID;
         label1.Text = $"{name}";
         label1.CssClass = "labels";
         panel1.Controls.Add(label1);
         panel1.Controls.Add(textbox1);

         return panel1;
      }

      public Panel CreateNewDropdownList(string name, string text, Dictionary<string, string> entries, string selectedValue, EventHandler selectedIndexChangedHandler)
      {
         // -- SNIPPET --
         //
         //<div style="margin-top: 5px;">
         //   <asp:Label ID="LabelListName" AssociatedControlID="DropDownListName" runat="server" Text="Verfügbare Listen" CssClass="labels"></asp:Label>
         //   <asp:DropDownList ID="DropDownListName" runat="server" CssClass="selectbox" OnSelectedIndexChanged="DropDownListName_SelectedIndexChanged">
         //      <asp:ListItem>Item 1</asp:ListItem>
         //      <asp:ListItem>Item 1</asp:ListItem>
         //      <asp:ListItem>Item 1</asp:ListItem>
         //      <asp:ListItem>Item 1</asp:ListItem>
         //      <asp:ListItem>Item 1</asp:ListItem>
         //      <asp:ListItem>Item 1</asp:ListItem>
         //   </asp:DropDownList>
         //</div>

         Panel panel1 = new Panel();
         panel1.Attributes["style"] = "margin-top: 5px;";

         DropDownList dropdownList = new DropDownList();
         dropdownList.ID = $"DropDownList{name}";
         dropdownList.CssClass = "selectbox";
         if (selectedIndexChangedHandler != null)
            dropdownList.SelectedIndexChanged += selectedIndexChangedHandler;
         bool found = false;
         foreach (var item in entries)
         {
            var newItem = new ListItem(item.Key, item.Value);
            if (item.Value == selectedValue)
            {
               newItem.Selected = true;
               found = true;
            }
            dropdownList.Items.Add(newItem);
         }
         if (string.IsNullOrWhiteSpace(selectedValue) || !found)
            dropdownList.Items[0].Selected = true;

         Label label1 = new Label();
         label1.AssociatedControlID = dropdownList.ID;
         label1.Text = text;
         panel1.Controls.Add(label1);
         panel1.Controls.Add(dropdownList);

         return panel1;
      }

      public Panel CreateNewH4Header(string text, string tooltip)
      {
         // -- SNIPPET --
         //
         //<div>
         //   <h4>
         //      <asp:Label ID="Label5" runat="server" Text="Sharepoint Zielliste(n)" ToolTip="Es können die Spalten aus der Exceldatei in mehreren SharePoint Listen gemapped werden."></asp:Label></h4>
         //</div>

         Panel panel1 = new Panel();
         Literal literalH4Begin = new Literal();
         literalH4Begin.Text = "<h4>";
         Literal literalH4End = new Literal();
         literalH4End.Text = "</h4>";
         Label label1 = new Label();
         label1.Text = text;
         label1.ToolTip = tooltip;
         panel1.Controls.Add(literalH4Begin);
         panel1.Controls.Add(label1);
         panel1.Controls.Add(literalH4End);

         return panel1;
      }

      public Button CreateNewAddButtonForSheet(string name, string buttonText, EventHandler eventHandler)
      {
         // -- SNIPPET --
         //
         //<div style="margin-top: 10px;">
         //   <asp:Label ID="Label6" AssociatedControlID="ButtonAddNewList" runat="server" Text="&nbsp;" CssClass="labels" />
         //   <asp:Button ID="ButtonAddNewList" runat="server" Text="Hinzufügen" OnClick="ButtonAddNewList_Click" CssClass="defaultButton" UseSubmitBehavior="false" />
         //</div>

         Panel panel1 = new Panel();
         panel1.Attributes["style"] = "margin-top: 10px;";
         Button button = new Button();
         button.ID = $"Button{name}";
         button.Text = buttonText;
         button.CssClass = "defaultButton";
         button.Click += eventHandler;
         Label label1 = new Label();
         label1.AssociatedControlID = button.ID;
         label1.CssClass = "labels";
         label1.Text = "&nbsp;";

         return button;
      }
   }
}