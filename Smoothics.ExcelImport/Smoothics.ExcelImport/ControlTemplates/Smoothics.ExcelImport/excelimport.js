﻿var Smoothics = Smoothics || {};

Smoothics.ExcelImport = (function ($) {
    'use strict';

    // put here private stuffs

    return {

        // public stuffs

        toggleTableContent: function (id) {
            var element = document.getElementById(id);
            if (element.style.height === "99%") {
                element.style.height = "0px";
            } else {
                element.style.height = "99%";
            }
        },
        alertMessage: function () {
            alert('Excel Import Control Message!');
        }

    }
})();
