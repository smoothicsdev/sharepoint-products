﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Serialization;

namespace Smoothics.ExcelImport.ControlTemplates.Smoothics.ExcelImport
{
    /// <summary>
    /// This control *dynamically* creates controls and displays the excel import configuration of a SP list. 
    /// Each excel import configuration is stored in a hidden list as an XML file.
    /// </summary>
    public partial class ExcelImportSettingsControl : UserControl
    {
        public static string HIDDEN_LIST_NAME = "Excel Import Configurations by Smoothics";
        private static int INVALID = -1;

        public bool IsNewMapping
        {
            get
            {
                if (ViewState["IsNewMapping"] == null)
                    ViewState["IsNewMapping"] = false;
                return (bool)ViewState["IsNewMapping"];
            }
            set
            {
                ViewState["IsNewMapping"] = value;
            }
        }

        public bool IsNewSheet
        {
            get
            {
                if (ViewState["IsNewSheet"] == null)
                    ViewState["IsNewSheet"] = false;
                return (bool)ViewState["IsNewSheet"];
            }
            set
            {
                ViewState["IsNewSheet"] = value;
            }
        }

        /// <summary>
        /// Displays the raw information string of the excel import configuration.
        /// </summary>
        public string ExcelMappingConfigRawXml
        {
            get
            {
                if (ViewState["ExcelMappingConfigRawXml"] != null)
                    return (string)ViewState["ExcelMappingConfigRawXml"];
                return null;
            }

            set
            {
                ViewState["ExcelMappingConfigRawXml"] = value;
            }
        }

        /// <summary>
        /// Displays the raw information of <see cref="ExcelMappingConfigRawXml"/> as a type.
        /// </summary>
        public SPListExcelImportConfiguration ExcelMappingConfig
        {
            get
            {
                if (ViewState["ExcelMappingConfig"] == null)
                {
                    var result = DeserializeRawExcelConfigMapping(ExcelMappingConfigRawXml);
                    ViewState["ExcelMappingConfig"] = result;
                }
                return (SPListExcelImportConfiguration)ViewState["ExcelMappingConfig"];
            }

            set
            {
                ViewState["ExcelMappingConfig"] = value;
            }
        }

        public int ActiveMappingConfigIndex
        {
            get
            {
                if (ViewState["ActiveMappingConfigIndex"] == null)
                    ViewState["ActiveMappingConfigIndex"] = 0;
                return (int)ViewState["ActiveMappingConfigIndex"];
            }
            set
            {
                ViewState["ActiveMappingConfigIndex"] = value;
            }
        }

        public int ActiveMappingSheetIndex
        {
            get
            {
                if (ViewState["ActiveMappingSheetIndex"] == null)
                    ViewState["ActiveMappingSheetIndex"] = 0;
                return (int)ViewState["ActiveMappingSheetIndex"];
            }
            set
            {
                ViewState["ActiveMappingSheetIndex"] = value;
            }
        }

        public List<KeyValuePair<string, string>> SharePointLists
        {
            get
            {
                if (ViewState["SharePointLists"] == null)
                    ViewState["SharePointLists"] = new List<KeyValuePair<string, string>>();
                return (List<KeyValuePair<string, string>>)ViewState["SharePointLists"];
            }
            set
            {
                ViewState["SharePointLists"] = value;
            }
        }

        public List<KeyValuePair<string, string>> SharePointContentTypes
        {
            get
            {
                if (ViewState["SharePointContentTypes"] == null)
                    ViewState["SharePointContentTypes"] = new List<KeyValuePair<string, string>>();
                return (List<KeyValuePair<string, string>>)ViewState["SharePointContentTypes"];
            }
            set
            {
                ViewState["SharePointContentTypes"] = value;
            }
        }


        public List<MappingValue> MappingValues
        {
            get
            {
                if (ViewState["MappingValues"] == null)
                    ViewState["MappingValues"] = new List<MappingValue>();
                return (List<MappingValue>)ViewState["MappingValues"];
            }
            set
            {
                ViewState["MappingValues"] = value;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            string cssPath = ResolveUrl("excelimport.css");
            string css = $"<link href=\"{cssPath}?v=42\" type=\"text/css\" rel=\"stylesheet\"></link>";
            Page.ClientScript.RegisterClientScriptBlock(typeof(ExcelImportSettingsControl), "_smoothics_excelimportcss", css, false);

            string jsPath = ResolveUrl("excelimport.js");
            string js = $"<script src=\"{jsPath}?v=42\" type=\"text/javascript\"></script>";
            Page.ClientScript.RegisterClientScriptBlock(typeof(ExcelImportSettingsControl), "_smoothics_excelimportjs", js, false);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CreateAndLoadXmlConfig();
                ReloadSharepointLists();
                BindSharePointLists(SharePointLists);
                ReloadContentTypes(string.Empty, SharePointLists[DropDownListSharePointLists.SelectedIndex].Value);
                BindSharePointContentTypes(SharePointContentTypes);
            }
            CreateMappingConfigButtons(ExcelMappingConfig);     //MAPPING HEADER
            CreateSheetConfigButtons(ExcelMappingConfig, ActiveMappingConfigIndex); //SHEET HEADER

            if (!IsNewMapping)
            {
                SetMappingValues();

                if (!IsNewSheet)
                {
                    SetMappingSheetValues(ExcelMappingConfig, ActiveMappingConfigIndex, ActiveMappingSheetIndex);
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        private void SetMappingSheetValues(SPListExcelImportConfiguration config, int activeMappingConfigIndex, int activeMappingSheetIndex)
        {
            if (config == null)
            {
                return;
            }

            if (PlaceHolderAllTargetListMappings.Controls.Count > 0)
                PlaceHolderAllTargetListMappings.Controls.Clear();
            if (config.MappingConfigurations[activeMappingConfigIndex].Sheets.Count > 0)
            {
                var sheet = config.MappingConfigurations[activeMappingConfigIndex].Sheets[activeMappingSheetIndex];
                TextBoxSheetName1.Text = sheet.Name;
                for (int i = 0; i < sheet.TargetListMappings.Count; i++)
                {
                    PlaceHolderAllTargetListMappings.Controls.Add(CreateNewTargetListMapping(i, sheet.TargetListMappings[i]));
                }
            }

        }

        /// <summary>
        /// Creates TargetListMapping dynamically (see html snippet in this method)
        /// </summary>
        /// <param name="consecutiveNr"></param>
        /// <param name="targetList"></param>
        /// <returns></returns>
        private Control CreateNewTargetListMapping(int consecutiveNr, TargetSPListMapping targetList)
        {
            #region html snippet
            //<div id="mapping1" class="mapping">
            //    <div class="header" onclick="Smoothics.ExcelImport.toggleHeight('content1', '280px')">
            //        <asp:Label ID="Label12" runat="server" Text="<b>Liste:</b> SharePoint Liste 1, <b>Inhaltstyp:</b> Type 1" />
            //        <div style="float: right;">
            //            <asp:LinkButton ID="LinkButtonRemoveMapping1" runat="server" OnClick="LinkButtonRemoveMapping_Click">Remove</asp:LinkButton>
            //        </div>
            //    </div>
            //    <div id="content1" class="content">
            //    <div class="mapping">
            //    <div class="table">
            //        <div class="rowHeader">
            //            <div class="cell">
            //                <asp:Label ID="Label9" runat="server" Text="Label 1" CssClass="label"></asp:Label>
            //            </div>
            //            <div class="cell">
            //                <asp:Label ID="Label12" runat="server" Text="Label 1" CssClass="label"></asp:Label>
            //            </div>
            //        </div>
            //        <div class="row">
            //            <div class="cell">
            //                <asp:Label ID="Label14" runat="server" Text="Label X" CssClass="label"></asp:Label>
            //            </div>
            //            <div class="cell">
            //                <asp:TextBox ID="TextBox1" runat="server" CssClass="textbox"></asp:TextBox>
            //            </div>
            //        </div>
            //        <div class="row">
            //            <div class="cell">
            //                <asp:Label ID="Label10" runat="server" Text="Label X" CssClass="label"></asp:Label>
            //            </div>
            //            <div class="cell">
            //                <asp:TextBox ID="TextBox3" runat="server" CssClass="textbox"></asp:TextBox>
            //            </div>
            //        </div>
            //    </div>
            //</div>

            #endregion

            Panel panelMapping = new Panel();
            panelMapping.ClientIDMode = ClientIDMode.Static;
            panelMapping.ID = $"smoothics_excelTargetListMapping_{consecutiveNr}";
            panelMapping.CssClass = "mapping";

            Panel panelContent = new Panel();
            panelContent.ClientIDMode = ClientIDMode.Static;
            panelContent.CssClass = "content";
            panelContent.ID = $"smoothics_panelContent_{consecutiveNr}";

            var table = CreateTable(consecutiveNr, targetList);
            panelContent.Controls.Add(table);

            string actualSite = SPContext.Current.Web.Name.Equals(targetList.SharePointSite) ? "Aktuelle Seite" : targetList.SharePointSite;
            Label label1 = new Label();
            label1.Text = $"<b>Site:&nbsp;</b>{actualSite}<br /><b>Liste:&nbsp;</b>{targetList.SharePointListName}, <b>Inhaltstyp:&nbsp;</b>{targetList.ContentTypeName}";
            Panel panel4 = new Panel();
            panel4.Attributes["style"] = "float: right;";
            LinkButton linkbutton = new LinkButton();
            linkbutton.Text = "Entfernen";
            linkbutton.Click += LinkButtonRemoveMapping_Click;
            HiddenField hiddenField1 = new HiddenField();
            hiddenField1.Value = panelMapping.ID;   //find this ID to remove
            panel4.Controls.Add(linkbutton);
            panel4.Controls.Add(hiddenField1);
            Panel panelHeader = new Panel();
            panelHeader.CssClass = "header";
            panelHeader.Attributes["onclick"] = $"Smoothics.ExcelImport.toggleTableContent('{panelContent.ID}')";
            panelHeader.Controls.Add(label1);
            panelHeader.Controls.Add(panel4);

            panelMapping.Controls.Add(panelHeader);
            panelMapping.Controls.Add(panelContent);

            return panelMapping;
        }

        private static Control CreateTable(int consecutiveNr, TargetSPListMapping targetList)
        {
            Panel panelTable = new Panel();
            panelTable.ClientIDMode = ClientIDMode.Static;
            panelTable.ID = $"smoothics_table_{consecutiveNr}";
            panelTable.CssClass = "table";              // table
            Panel panelTableHeader = new Panel();
            panelTableHeader.CssClass = "rowHeader";    // table header
            Panel panelTableHeaderCell1 = new Panel();
            panelTableHeaderCell1.CssClass = "cell";
            Label labelTableHeader1 = new Label();
            labelTableHeader1.Text = "Inhaltstyp";
            labelTableHeader1.CssClass = "label";
            panelTableHeaderCell1.Controls.Add(labelTableHeader1);
            Panel panelTableHeaderCell2 = new Panel();
            panelTableHeaderCell2.CssClass = "cell";
            Label labelTableHeader2 = new Label();
            labelTableHeader2.Text = "Excel-Spalte";
            labelTableHeader2.CssClass = "label";
            panelTableHeaderCell2.Controls.Add(labelTableHeader2);
            panelTableHeader.Controls.Add(panelTableHeaderCell1);
            panelTableHeader.Controls.Add(panelTableHeaderCell2);
            panelTable.Controls.Add(panelTableHeader);

            for (int i = 0; i < targetList.MappingValues.Count; i++)
            {
                Panel panelRow = new Panel();
                panelRow.ClientIDMode = ClientIDMode.Static;
                panelRow.ID = $"{panelTable.ID}_row_{i}";
                panelRow.CssClass = "row";

                Panel panelCell1 = new Panel();
                panelCell1.CssClass = "cell";
                Label labelText1 = new Label();
                labelText1.Text = targetList.MappingValues[i].ContentTypeName;
                labelText1.CssClass = "label";
                HiddenField hiddenContentTypeId = new HiddenField();
                hiddenContentTypeId.ClientIDMode = ClientIDMode.Static;
                hiddenContentTypeId.ID = $"{panelTable.ID}_HiddenFieldContentTypeId_{i}";
                hiddenContentTypeId.Value = targetList.MappingValues[i].ContentTypeId;
                panelCell1.Controls.Add(labelText1);
                panelCell1.Controls.Add(hiddenContentTypeId);

                Panel panelCell2 = new Panel();
                panelCell2.CssClass = "cell";
                TextBox txtBox = new TextBox();
                txtBox.ClientIDMode = ClientIDMode.Static;
                txtBox.ID = $"{panelTable.ID}_TextBoxExcelColumn_{i}";
                txtBox.Text = targetList.MappingValues[i].ExcelColumn;
                txtBox.CssClass = "textbox";
                panelCell2.Controls.Add(txtBox);

                panelRow.Controls.Add(panelCell1);
                panelRow.Controls.Add(panelCell2);

                panelTable.Controls.Add(panelRow);
            }
            return panelTable;
        }

        private void BindSharePointLists(List<KeyValuePair<string, string>> sharePointLists)
        {
            if (sharePointLists == null)
                return;

            //TODO: don't display already defined list mappings => one mapping contains: one list and one contenttype
            var items = sharePointLists
                            .Where(elem => !elem.Value.Equals(SPContext.Current.ListId.ToString()))
                            .Select(elem => new ListItem(elem.Key, elem.Value))
                            .ToArray();
            if (items.Length > 0)
                items[0].Selected = true;
            DropDownListSharePointLists.DataSource = items;
            DropDownListSharePointLists.DataBind();
        }

        private void ReloadSharepointLists()
        {
            ReloadSharepointLists(string.Empty);
        }

        /// <summary>
        /// Loads all lists (title, id) into the ViewState 'SharePointLists'. Call BindSharePointLists(...) afterwards.
        /// </summary>
        /// <param name="siteUrl"></param>
        private void ReloadSharepointLists(string siteUrl)
        {
            if (SharePointLists.Count > 0)
                SharePointLists.Clear();

            IEnumerator listEnumerator = null;
            if (string.IsNullOrWhiteSpace(siteUrl))
            {
                // load current site lists

                PanelSharePointSiteOtherBox.Visible = false;
                listEnumerator = SPContext.Current.Web.Lists.GetEnumerator();
                while (listEnumerator.MoveNext())
                {
                    SPList listItem = (SPList)listEnumerator.Current;
                    if (listItem.Hidden)
                        continue;
                    SharePointLists.Add(new KeyValuePair<string, string>(listItem.Title, listItem.ID.ToString()));
                }
            }
            else
            {
                // load other site lists
                PanelSharePointSiteOtherBox.Visible = true;
                try
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            listEnumerator = web.Lists.GetEnumerator();
                            while (listEnumerator.MoveNext())
                            {
                                SPList item = (SPList)listEnumerator.Current;
                                if (item.Hidden)
                                    continue;
                                SharePointLists.Add(new KeyValuePair<string, string>(item.Title, item.ID.ToString()));
                            }
                            LabelTestAndLoadResult.ForeColor = System.Drawing.Color.Green;
                            LabelTestAndLoadResult.Text = $"Loading {siteUrl} succeeded";
                        }
                    }
                }
                catch (Exception ex)
                {
                    // TODO: log ex
                    LabelTestAndLoadResult.ForeColor = System.Drawing.Color.Red;
                    LabelTestAndLoadResult.Text = $"Error loading {siteUrl}";
                }
            }
        }

        private void ReloadContentTypes(string siteUrl, string listId)
        {
            if (SharePointContentTypes.Count > 0)
                SharePointContentTypes.Clear();

            SPContentTypeCollection listCollection = null;
            if (string.IsNullOrWhiteSpace(siteUrl))
            {
                // listId belongs to current site
                listCollection = SPContext.Current.Web.Lists[new Guid(listId)].ContentTypes;
            }
            else
            {
                // listId belongs to other site

                try
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            listCollection = web.Lists[new Guid(listId)].ContentTypes;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // TODO: log ex
                }
            }

            if (listCollection != null && listCollection.Count > 0)
            {
                var listEnumerator = listCollection.GetTypedEnumerator<SPContentType>();
                while (listEnumerator.MoveNext())
                {
                    var item = listEnumerator.Current;
                    if (item.Hidden)
                        continue;
                    SharePointContentTypes.Add(new KeyValuePair<string, string>(item.Name, item.Id.ToString()));
                }
            }
        }

        private void BindSharePointContentTypes(List<KeyValuePair<string, string>> sharePointContentTypes)
        {
            if (sharePointContentTypes == null)
                return;

            var items = sharePointContentTypes
                .Select(elem => new ListItem(elem.Key, elem.Value))
                .ToArray();
            if (items.Length > 0)
                items[0].Selected = true;
            DropDownListContentType.DataSource = items;
            DropDownListContentType.DataBind();
        }

        private void CreateSheetConfigButtons(SPListExcelImportConfiguration config, int activeMappingConfigIndex)
        {
            // SHEET HEADER
            {
                if (PlaceHolderSheetConfigButtons.Controls.Count > 0)
                    PlaceHolderSheetConfigButtons.Controls.Clear();
                for (int i = 0; i < config.MappingConfigurations[activeMappingConfigIndex].Sheets.Count; i++)
                {
                    PlaceHolderSheetConfigButtons.Controls.Add(CreateButton(config.MappingConfigurations[activeMappingConfigIndex].Sheets[i].Name, string.Empty, SheetConfig_Click, "sheetButton_" + i.ToString()));
                }
                PlaceHolderSheetConfigButtons.Controls.Add(CreateButton("+", "Neue Excelsheet-Konfiguration anlegen", SheetConfig_Click, "sheetButton_add"));
            }
        }

        private void CreateMappingConfigButtons(SPListExcelImportConfiguration config)
        {
            // MAPPING HEADER
            if (config != null)
            {
                if (PlaceHolderMappingConfigButtons.Controls.Count > 0)
                    PlaceHolderMappingConfigButtons.Controls.Clear();
                for (int i = 0; i < config.MappingConfigurations.Count; i++)
                {
                    PlaceHolderMappingConfigButtons.Controls.Add(CreateButton(config.MappingConfigurations[i].Name, string.Empty, MappingConfig_Click, "mappingButton_" + i.ToString()));
                }
                PlaceHolderMappingConfigButtons.Controls.Add(CreateButton("+", "Neue Excel-Mapping Konfiguration anlegen", MappingConfig_Click, "mappingButton_add"));
            }
        }

        private void CreateAndLoadXmlConfig()
        {
            SPList configList = null;
            var web = SPContext.Current.Web;
            if (web.Lists.TryGetList(HIDDEN_LIST_NAME) == null)
            {
                // create a hidden document library for all configuration(s)

                // 1. create hidden document library
                SPContext.Current.Web.AllowUnsafeUpdates = true;
                var guid = web.Lists.Add(HIDDEN_LIST_NAME, string.Empty, SPListTemplateType.DocumentLibrary);
                var newHiddenList = web.Lists.GetList(guid, false);
                newHiddenList.Hidden = true;
                newHiddenList.EnableVersioning = false;
                SPField listIdField = (SPFieldText)newHiddenList.Fields.CreateNewField(SPFieldType.Text.ToString(), "ListIDColumn");
                newHiddenList.Fields.Add(listIdField);
                newHiddenList.Update();

                // 2. replace internal field name with displayname (== display title)
                listIdField = newHiddenList.Fields["ListIDColumn"] as SPField;
                listIdField.Title = "List ID";
                listIdField.Update();

                // 3. add new field to default view
                var defaultView = newHiddenList.DefaultView;
                defaultView.ViewFields.Add(listIdField);
                defaultView.Update();

                // 4. reload default view by reassigning again(!) to position our new field at the first place
                defaultView = newHiddenList.DefaultView;
                defaultView.ViewFields.MoveFieldTo(listIdField.InternalName, 0);
                defaultView.Update();

                SPContext.Current.Web.AllowUnsafeUpdates = false;
                configList = newHiddenList;
            }
            else
            {
                configList = web.Lists[HIDDEN_LIST_NAME];
            }
            //var url = web.Site.MakeFullUrl(configList.RootFolder.ServerRelativeUrl);

            SPQuery query = new SPQuery();
            query.Query = $"<Where><Eq><FieldRef Name='ListIDColumn' /><Value Type='Text'>{SPContext.Current.ListId}</Value></Eq></Where>";
            var result = configList.GetItems(query);
            if (result.Count == 0)
            {
                //TODO: what if no configuration file found?
                IsNewMapping = true;
            }
            else if (result.Count == 1)
            {
                //excel import exists for this list
                var stream = result[0].File.OpenBinaryStream();
                var deSerializer = new XmlSerializer(typeof(SPListExcelImportConfiguration));
                using (var reader = new StreamReader(stream))
                {
                    ExcelMappingConfigRawXml = reader.ReadToEnd();
                }
            }
            else
            {
                // TODO: logging! multiple results for ListId=.... found!
            }
        }

        private void SetMappingValues()
        {
            if (ExcelMappingConfig != null && (ActiveMappingConfigIndex < ExcelMappingConfig.MappingConfigurations.Count))
            {
                SetMappingConfigValues(ExcelMappingConfig.MappingConfigurations[ActiveMappingConfigIndex]);
            }
        }

        private void SetMappingConfigValues(MappingConfiguration configuration)
        {
            TextBoxMappingConfiguration1.Text = configuration.Name;
            var file = Path.HasExtension(configuration.ExcelFileName) ? Path.GetFileNameWithoutExtension(configuration.ExcelFileName) : Path.GetFileName(configuration.ExcelFileName);
            TextBoxExcelFilename1.Text = file;
            CheckBoxClearDataBeforeImport1.Checked = configuration.ClearDataBeforeImport;
        }

        private Control CreateButton(string text, string tooltip, EventHandler eventHandler, string buttonId)
        {
            Button button = new Button();
            button.ClientIDMode = ClientIDMode.Static;
            button.ID = buttonId;
            button.Text = text;
            button.ToolTip = tooltip;
            button.UseSubmitBehavior = false;   //to render: input[type="button"] => see also its css!
            button.Click += eventHandler;
            return button;
        }

        protected void MappingConfig_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if ("+".Equals(button.Text))
            {
                PanelSheetConfigurations.Visible = false;   // no sheet configurations until valid input (+ Save) or loading existing config
                IsNewMapping = true;
                TextBoxMappingConfiguration1.Text = string.Empty;
                TextBoxExcelFilename1.Text = string.Empty;
                CheckBoxClearDataBeforeImport1.Checked = false;
                TextBoxSheetName1.Text = string.Empty;
                PlaceHolderAllTargetListMappings.Controls.Clear();
                PlaceHolderMappingConfigButtons.Controls.Remove(button);    // show no '+' button
                WriteStatusMessage("Bitte den Namen der Konfiguration eingeben und dann auf 'Speichern' klicken, bevor Sie fortfahren.", StatusLevel.Info);
            }
            else
            {
                // load configuration from xml
                ActiveMappingConfigIndex = GetActiveMappingIndexBy(button.Text);
                if (ActiveMappingConfigIndex == INVALID)
                    return; //TODO: logging?
                ActiveMappingSheetIndex = 0;
                PanelSheetConfigurations.Visible = true;
                IsNewMapping = false;
                CreateMappingConfigButtons(ExcelMappingConfig);     //MAPPING HEADER
                CreateSheetConfigButtons(ExcelMappingConfig, ActiveMappingConfigIndex); //SHEET HEADER
                SetMappingValues();
                SetMappingSheetValues(ExcelMappingConfig, ActiveMappingConfigIndex, ActiveMappingSheetIndex);
            }
        }

        private int GetActiveMappingIndexBy(string text)
        {
            for (int i = 0; i < ExcelMappingConfig.MappingConfigurations.Count; i++)
            {
                if (ExcelMappingConfig.MappingConfigurations[i].Name.Equals(text))
                {
                    return i;
                }
            }
            return INVALID;
        }

        protected void SheetConfig_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if ("+".Equals(button.Text))
            {
                IsNewSheet = true;
                PlaceHolderSheetConfigButtons.Controls.Remove(button);    // show no '+' button
                WriteStatusMessage("Bitte den Namen der Sheet-Konfiguration eingeben und dann auf 'Speichern' klicken, bevor Sie fortfahren.", StatusLevel.Info);
                PanelTargetListConfigurations.Visible = false;
                TextBoxSheetName1.Text = string.Empty;
            }
            else
            {
                // load configuration from xml
                ActiveMappingSheetIndex = GetActiveSheetIndexBy(button.Text);
                if (ActiveMappingSheetIndex == INVALID)
                    return; //TODO: logging?
                IsNewSheet = false;
                PanelTargetListConfigurations.Visible = true;
                CreateSheetConfigButtons(ExcelMappingConfig, ActiveMappingConfigIndex); //SHEET HEADER
                SetMappingSheetValues(ExcelMappingConfig, ActiveMappingConfigIndex, ActiveMappingSheetIndex);
            }
        }

        private int GetActiveSheetIndexBy(string text)
        {
            for (int i = 0; i < ExcelMappingConfig.MappingConfigurations[ActiveMappingConfigIndex].Sheets.Count; i++)
            {
                if (ExcelMappingConfig.MappingConfigurations[ActiveMappingConfigIndex].Sheets[i].Name.Equals(text))
                {
                    return i;
                }
            }
            return INVALID;
        }


        protected void DropDownListSharePointLists_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SharePointLists.Count == 0)
                return;

            if (DropDownListSharePointSiteSelection.SelectedIndex == 1)
            {
                // other site
                ReloadContentTypes(TextBoxSharePointOtherSite.Text, SharePointLists[DropDownListSharePointLists.SelectedIndex].Value);
            }
            else
            {
                // current site
                ReloadContentTypes(string.Empty, SharePointLists[DropDownListSharePointLists.SelectedIndex].Value);
            }
            BindSharePointContentTypes(SharePointContentTypes);
        }

        protected void LinkButtonRemoveMapping_Click(object sender, EventArgs e)
        {
            //TODO: remove Mapping n
            LinkButton btn = (LinkButton)sender;
        }

        protected void ButtonAddNewSheetMappingList_Click(object sender, EventArgs e)
        {
            // TODO: excel sheet mapping list
            var selectedSite = DropDownListSharePointSiteSelection.SelectedValue.Equals("0") ?
                                                                SPContext.Current.Web.Site.Url :
                                                                TextBoxSharePointOtherSite.Text;
            if (string.IsNullOrWhiteSpace(selectedSite))
            {
                WriteStatusMessage("Keine <b>Site</b> angegeben", StatusLevel.Error);
                return;
            }

            if (DropDownListSharePointLists.SelectedIndex == -1)
            {
                WriteStatusMessage("Keine <b>Liste</b> angegeben", StatusLevel.Error);
                return;
            }
            KeyValuePair<string, string> selectedListItem = SharePointLists[DropDownListSharePointLists.SelectedIndex];


            if (DropDownListContentType.SelectedIndex == -1)
            {
                WriteStatusMessage("Kein <b>Inhaltstyp</b> angegeben", StatusLevel.Error);
                return;
            }
            KeyValuePair<string, string> selectedContentType = SharePointContentTypes[DropDownListContentType.SelectedIndex];


            // TODO: speichern im aktuellen XML Config

            WriteStatusMessage($"[Site: {selectedSite}, Liste: {selectedListItem.Key}, Inhaltstyp: {selectedContentType.Key}] wurde <b>erfolgreich hinzugefügt</b>. Bitte nicht vergessen auf '<b>Speichern</b>' klicken.", StatusLevel.Success);
        }

        private void WriteStatusMessage(string htmlMessage, StatusLevel error)
        {
            SPPageStatusSetter setter1 = new SPPageStatusSetter() { Visible = true };
            switch (error)
            {
                case StatusLevel.Info:
                    setter1.AddStatus("Info", htmlMessage, SPPageStatusColor.Yellow);
                    break;
                case StatusLevel.Success:
                    setter1.AddStatus("Success", htmlMessage, SPPageStatusColor.Green);
                    break;
                case StatusLevel.Warning:
                    setter1.AddStatus("Warning", htmlMessage, SPPageStatusColor.Yellow);
                    break;
                case StatusLevel.Error:
                    setter1.AddStatus("Error", htmlMessage, SPPageStatusColor.Red);
                    break;
                case StatusLevel.Fatal:
                    setter1.AddStatus("Fatal", htmlMessage, SPPageStatusColor.Blue);
                    break;
            }
            this.Controls.Add(setter1);
        }

        protected void ButtonTestAndLoadSharePointSite_Click(object sender, EventArgs e)
        {
            SharePointLists.Clear();
            SharePointContentTypes.Clear();
            var otherSiteUrl = TextBoxSharePointOtherSite.Text.Trim();
            if (string.IsNullOrWhiteSpace(otherSiteUrl))
                return;

            ReloadSharepointLists(otherSiteUrl);
            BindSharePointLists(SharePointLists);
            if (SharePointLists.Count > 0)
            {
                ReloadContentTypes(otherSiteUrl, SharePointLists[0].Value);
            }
            BindSharePointContentTypes(SharePointContentTypes);
        }

        protected void DropDownListSharePointSiteSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            var isOtherSite = DropDownListSharePointSiteSelection.SelectedValue.Equals("1");
            TextBoxSharePointOtherSite.Text = string.Empty;
            LabelTestAndLoadResult.Text = string.Empty;
            SharePointLists.Clear();
            SharePointContentTypes.Clear();
            if (isOtherSite)
            {
                PanelSharePointSiteOtherBox.Visible = true;
                TextBoxSharePointOtherSite.Focus();
            }
            else
            {
                PanelSharePointSiteOtherBox.Visible = false;
                ReloadSharepointLists();
            }
            BindSharePointLists(SharePointLists);

            if (SharePointLists.Count > 0)
            {
                if (isOtherSite)
                {
                    ReloadContentTypes(TextBoxSharePointOtherSite.Text, SharePointLists[0].Value);
                }
                else
                {
                    ReloadContentTypes(string.Empty, SharePointLists[0].Value);
                }
            }
            BindSharePointContentTypes(SharePointContentTypes);
        }

        protected void ButtonSaveOrCancel_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (button.ID.Equals("ButtonCancel"))
            {
                SPUtility.Redirect(SPContext.Current.Web.Url, SPRedirectFlags.UseSource, HttpContext.Current);
                return;
            }

            if (IsNewMapping)
            {
                var configName = TextBoxMappingConfiguration1.Text.Trim();
                var excelFileName = TextBoxExcelFilename1.Text.Trim();
                var clearBeforeImport = CheckBoxClearDataBeforeImport1.Checked;

                if (string.IsNullOrWhiteSpace(configName))
                {
                    WriteStatusMessage("Bitte Name der Konfiguration angeben", StatusLevel.Error);
                    return;
                }
                var duplicateConfig = ExcelMappingConfig.MappingConfigurations.Where(elem => elem.Name.Equals(configName)).SingleOrDefault();
                if (duplicateConfig != null)
                {
                    WriteStatusMessage("Konfigurationsname bereits vergeben. Bitte andere Bezeichnung wählen.", StatusLevel.Error);
                    return;
                }
                if (string.IsNullOrWhiteSpace(excelFileName))
                {
                    WriteStatusMessage("Bitte Name der Exceldatei angeben.", StatusLevel.Error);
                    return;
                }
                var excelFileConfig = ExcelMappingConfig.MappingConfigurations.Where(elem => elem.ExcelFileName.Equals(excelFileName)).SingleOrDefault();
                if(excelFileConfig != null)
                {
                    var str = $"Exceldatei wird bereits in der Konfiguration '{excelFileConfig.Name}' verwendet. Benutzen Sie stattdessen die Ziellisten Definitionen, falls Sie einen Excel-Import in multiple Listen haben wollen.";
                    WriteStatusMessage(str, StatusLevel.Warning);
                }

                var newConfig = new MappingConfiguration();
                newConfig.Name = configName;
                newConfig.ExcelFileName = excelFileName;
                newConfig.ClearDataBeforeImport = clearBeforeImport;
                if (!string.IsNullOrWhiteSpace(ExcelMappingConfigRawXml))
                {
                    var result = DeserializeRawExcelConfigMapping(ExcelMappingConfigRawXml);
                    result.MappingConfigurations.Add(newConfig);
                    ExcelMappingConfigRawXml = SerializeExcelConfigMappingToRaw(result);
                    ActiveMappingConfigIndex = result.MappingConfigurations.Count - 1;
                    ActiveMappingSheetIndex = 0;
                    PanelSheetConfigurations.Visible = true;
                    IsNewMapping = false;
                    CreateMappingConfigButtons(result);     //MAPPING HEADER
                    CreateSheetConfigButtons(result, ActiveMappingConfigIndex); //SHEET HEADER
                    SetMappingValues();
                    SetMappingSheetValues(result, ActiveMappingConfigIndex, ActiveMappingSheetIndex);
                    ExcelMappingConfig = null;  //reset ViewState
                }
            }

            if (IsNewSheet)
            {
                var sheetName = TextBoxSheetName1.Text;
                if (string.IsNullOrWhiteSpace(sheetName))
                {
                    WriteStatusMessage("Kein Name für das neue Sheet angegeben.", StatusLevel.Error);
                }
            }

            //TODO: in die HiddenList wieder speichern: aber nur wenn mind. eine Sheet-Definition existiert!
        }

        private SPListExcelImportConfiguration DeserializeRawExcelConfigMapping(string excelMappingConfigRawXml)
        {
            SPListExcelImportConfiguration result = null;
            if (!string.IsNullOrWhiteSpace(excelMappingConfigRawXml))
            {
                var deSerializer = new XmlSerializer(typeof(SPListExcelImportConfiguration));
                using (TextReader reader = new StringReader(excelMappingConfigRawXml))
                {
                    result = (SPListExcelImportConfiguration)deSerializer.Deserialize(reader);
                }
            }
            return result;
        }

        private string SerializeExcelConfigMappingToRaw(SPListExcelImportConfiguration config)
        {
            if (config != null)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SPListExcelImportConfiguration));
                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                serializer.Serialize(sw, config);
                string xmlResult = sw.GetStringBuilder().ToString();
                return xmlResult;
            }
            return null;
        }

        private SPListExcelImportConfiguration CreateNewSPListExcelConfiguration()
        {
            throw new NotImplementedException();
        }
    }
}
