﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Smoothics.ExcelImport.ControlTemplates.Smoothics.ExcelImport
{
   [Serializable]
   [XmlRoot(ElementName = "Sheet")]
   public class SheetConfiguration
   {
      [XmlAttribute]
      public string Name;
      [XmlArrayItem("TargetListMapping")]
      public List<TargetSPListMapping> TargetListMappings = new List<TargetSPListMapping>();
   }
}