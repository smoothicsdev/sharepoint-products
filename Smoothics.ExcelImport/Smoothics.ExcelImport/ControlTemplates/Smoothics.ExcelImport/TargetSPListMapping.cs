﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Smoothics.ExcelImport.ControlTemplates.Smoothics.ExcelImport
{
    [Serializable]
    [XmlRoot(ElementName = "TargetListMapping")]
    public class TargetSPListMapping
    {
        [XmlAttribute]
        public string SharePointSite;

        [XmlAttribute]
        public string SharePointListId;

        [XmlAttribute]
        public string SharePointListName;

        [XmlAttribute]
        public string ContentTypeId;

        [XmlAttribute]
        public string ContentTypeName;

        [XmlArrayItem("MappingValue")]
        public List<MappingValue> MappingValues = new List<MappingValue>();
    }
}