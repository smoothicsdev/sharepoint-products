﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Smoothics.ExcelImport.ControlTemplates.Smoothics.ExcelImport
{
   [Serializable]
   [XmlRoot(ElementName = "SPListExcelImportConfiguration")]
   public class SPListExcelImportConfiguration
   {
      public string SPListId;
      [XmlArrayItem("MappingConfiguration")]
      public List<MappingConfiguration> MappingConfigurations = new List<MappingConfiguration>();
   }
}