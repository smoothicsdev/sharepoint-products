﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Smoothics.ExcelImport.ControlTemplates.Smoothics.ExcelImport
{
   [Serializable]
   [XmlRoot(ElementName = "MappingConfiguration")]
   public class MappingConfiguration
   {
      [XmlAttribute]
      public string Name;

      [XmlAttribute]
      public string ExcelFileName;

      [XmlAttribute]
      public bool ClearDataBeforeImport;

      [XmlArrayItem("Sheet")]
      public List<SheetConfiguration> Sheets = new List<SheetConfiguration>();
   }
}