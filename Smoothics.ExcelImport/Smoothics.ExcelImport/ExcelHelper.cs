﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smoothics.ExcelImport
{
    public class ExcelHelper : IDisposable
    {
        private static string[] chars = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };


        public XLWorkbook Workbook { get; set; }
        public int MaxSheets { get; }


        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Workbook != null) Workbook.Dispose();
            }
        }

        #endregion


        public ExcelHelper(string excelFilePath)
        {
            Workbook = new XLWorkbook(excelFilePath);
            MaxSheets = Workbook.Worksheets.Count;
        }

        public ExcelHelper(Stream filestream)
        {
            Workbook = new XLWorkbook(filestream);
        }

        public static string GetColumnNameByIndex(int columnIndex)
        {
            columnIndex -= 1; //adjust so it matches 0-indexed array rather than 1-indexed column

            int quotient = columnIndex / 26;
            if (quotient > 0)
                return GetColumnNameByIndex(quotient) + chars[columnIndex % 26].ToString();
            else
                return chars[columnIndex % 26].ToString();
        }

        public static int GetIndexByColumnName(string excelColumnName)
        {
            if (string.IsNullOrWhiteSpace(excelColumnName))
                return 0;

            int idx = 1;
            while (idx < int.MaxValue)
            {
                var excelColumn = GetColumnNameByIndex(idx);
                if (excelColumn == excelColumnName)
                    break;
                idx++;
            }
            return idx;
        }

        #region GetMaxUsedColumns()

        public int GetMaxUsedColumns(string sheetName)
        {
            return Workbook.Worksheet(sheetName).LastColumnUsed().ColumnNumber();
        }

        public int GetMaxUsedColumns(int sheetPosition)
        {
            return Workbook.Worksheet(sheetPosition).LastColumnUsed().ColumnNumber();
        }

        #endregion


        #region GetMaxUsedRows()

        public int GetMaxUsedRows(string sheetName)
        {
            return Workbook.Worksheet(sheetName).LastRowUsed().RowNumber();
        }

        public int GetMaxUsedRows(int sheetPosition)
        {
            return Workbook.Worksheet(sheetPosition).LastRowUsed().RowNumber();
        }

        #endregion


        #region GetCellValue()

        public string GetCellValue(string sheetName, int row, int column)
        {
            string val = string.Empty;
            var result = Workbook.Worksheet(sheetName).Cell(row, column).TryGetValue(out val);
            return val;
        }

        public string GetCellValue(string sheetName, string excelCell = "A1")
        {
            string val = string.Empty;
            var result = Workbook.Worksheet(sheetName).Cell(excelCell).TryGetValue(out val);
            return val;
        }

        public string GetCellValue(int sheetPosition, int row, int column)
        {
            string val = string.Empty;
            var result = Workbook.Worksheet(sheetPosition).Cell(row, column).TryGetValue(out val);
            return val;
        }

        public string GetCellValue(int sheetPosition, string excelCell = "A1")
        {
            string val = string.Empty;
            var result = Workbook.Worksheet(sheetPosition).Cell(excelCell).TryGetValue(out val);
            return val;
        }

        #endregion


        #region GetCellValueAsBoolean()

        public bool GetCellValueAsBoolean(string sheetName, int row, int column)
        {
            return TryGetBoolean(Workbook.Worksheet(sheetName).Cell(row, column));
        }

        public bool GetCellValueAsBoolean(int sheetPosition, int row, int column)
        {
            return TryGetBoolean(Workbook.Worksheet(sheetPosition).Cell(row, column));
        }

        public bool GetCellValueAsBoolean(string sheetPosition, string excelColumn)
        {
            return TryGetBoolean(Workbook.Worksheet(sheetPosition).Cell(excelColumn));
        }

        public bool GetCellValueAsBoolean(int sheetPosition, string excelColumn)
        {
            return TryGetBoolean(Workbook.Worksheet(sheetPosition).Cell(excelColumn));
        }

        #endregion


        #region GetCellValueAsNumber()

        public float GetCellValueAsNumber(string sheetName, int row, int column)
        {
            float val = 0.0f;
            var result = Workbook.Worksheet(sheetName).Cell(row, column).TryGetValue(out val);
            return val;
        }

        public float GetCellValueAsNumber(int sheetPosition, int row, int column)
        {
            float val = 0.0f;
            var result = Workbook.Worksheet(sheetPosition).Cell(row, column).TryGetValue(out val);
            return val;
        }

        public float GetCellValueAsNumber(string sheetName, string excelColumn)
        {
            float val = 0.0f;
            var result = Workbook.Worksheet(sheetName).Cell(excelColumn).TryGetValue(out val);
            return val;
        }

        public float GetCellValueAsNumber(int sheetPosition, string excelColumn)
        {
            float val = 0.0f;
            var result = Workbook.Worksheet(sheetPosition).Cell(excelColumn).TryGetValue(out val);
            return val;
        }

        #endregion


        #region GetCellValueAsDateTime()

        public DateTime GetCellValueAsDateTime(string sheetName, int row, int column)
        {
            DateTime val;
            var result = Workbook.Worksheet(sheetName).Cell(row, column).TryGetValue(out val);
            return val;
        }

        public DateTime GetCellValueAsDateTime(int sheetPosition, int row, int column)
        {
            DateTime val;
            var result = Workbook.Worksheet(sheetPosition).Cell(row, column).TryGetValue(out val);
            return val;
        }

        public DateTime GetCellValueAsDateTime(string sheetName, string excelColumn)
        {
            DateTime val;
            var result = Workbook.Worksheet(sheetName).Cell(excelColumn).TryGetValue(out val);
            return val;
        }

        public DateTime GetCellValueAsDateTime(int sheetPosition, string excelColumn)
        {
            DateTime val;
            var result = Workbook.Worksheet(sheetPosition).Cell(excelColumn).TryGetValue(out val);
            return val;
        }

        #endregion


        #region IsCellValueBoolean

        public bool IsCellValueBoolean(int sheetPosition, int row, int column)
        {
            return TryGetBoolean(Workbook.Worksheet(sheetPosition).Cell(row, column));
        }

        public bool IsCellValueBoolean(int sheetPosition, string excelCell = "A1")
        {
            return TryGetBoolean(Workbook.Worksheet(sheetPosition).Cell(excelCell));
        }

        public bool IsCellValueBoolean(string sheetName, int row, int column)
        {
            return TryGetBoolean(Workbook.Worksheet(sheetName).Cell(row, column));
        }

        public bool IsCellValueBoolean(string sheetName, string excelCell = "A1")
        {
            return TryGetBoolean(Workbook.Worksheet(sheetName).Cell(excelCell));
        }

        private bool TryGetBoolean(IXLCell cell)
        {
            bool val = false;
            if (!cell.TryGetValue(out val))
            {
                if (cell.ShareString)
                {
                    var boolStr = ((string)cell.Value).ToLowerInvariant().Trim();
                    if (boolStr.Equals("no") || boolStr.Equals("nein") || string.IsNullOrWhiteSpace(boolStr))
                    {
                        val = false;
                    }
                    else
                    {
                        val = true;
                    }
                }
            }
            return val;
        }

        #endregion


        #region IsCellValueNumber

        public bool IsCellValueNumber(int sheetPosition, int row, int column)
        {
            return DetermineIfNumber(Workbook.Worksheet(sheetPosition).Cell(row, column));
        }

        public bool IsCellValueNumber(int sheetPosition, string excelCell = "A1")
        {
            return DetermineIfNumber(Workbook.Worksheet(sheetPosition).Cell(excelCell));
        }

        public bool IsCellValueNumber(string sheetName, int row, int column)
        {
            return DetermineIfNumber(Workbook.Worksheet(sheetName).Cell(row, column));
        }

        public bool IsCellValueNumber(string sheetName, string excelCell = "A1")
        {
            return DetermineIfNumber(Workbook.Worksheet(sheetName).Cell(excelCell));
        }

        private bool DetermineIfNumber(IXLCell cell)
        {
            if (cell.DataType == XLCellValues.Number)
                return true;

            float val = 0.0f;
            return cell.TryGetValue(out val);
        }

        #endregion


        #region IsCellValueDateTime

        public bool IsCellValueDateTime(int sheetPosition, int row, int column)
        {
            return DetermineIfDateTime(Workbook.Worksheet(sheetPosition).Cell(row, column));
        }

        public bool IsCellValueDateTime(int sheetPosition, string excelCell = "A1")
        {
            return DetermineIfDateTime(Workbook.Worksheet(sheetPosition).Cell(excelCell));
        }

        public bool IsCellValueDateTime(string sheetName, int row, int column)
        {
            return DetermineIfDateTime(Workbook.Worksheet(sheetName).Cell(row, column));
        }

        public bool IsCellValueDateTime(string sheetName, string excelCell = "A1")
        {
            return DetermineIfDateTime(Workbook.Worksheet(sheetName).Cell(excelCell));
        }

        private bool DetermineIfDateTime(IXLCell cell)
        {
            if (cell.DataType == XLCellValues.DateTime)
                return true;

            DateTime date;
            return cell.TryGetValue(out date);
        }

        #endregion

    }
}
