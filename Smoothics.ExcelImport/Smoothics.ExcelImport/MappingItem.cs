﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smoothics.ExcelImport
{
    [Serializable]
    public class MappingItem
    {
        public string ContentTypeColumnStaticName { get; set; }
        public string ContentTypeColumnName { get; set; }
        public string ExcelColumnName { get; set; }

        private int excelColumnIndex = -1;
        public int ExcelColumnIndex
        {
            get
            {
                if(excelColumnIndex == -1)
                {
                    excelColumnIndex = ExcelHelper.GetIndexByColumnName(ExcelColumnName);
                }
                return excelColumnIndex;
            }
        }

        public bool ClearContentBeforeImport { get; set; }
    }
}
