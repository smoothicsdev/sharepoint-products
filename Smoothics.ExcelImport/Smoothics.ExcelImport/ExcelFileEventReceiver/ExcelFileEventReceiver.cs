﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;
using System.IO;
using System.Web;
using System.Linq;
using System.Collections.Generic;

namespace Smoothics.ExcelImport
{
    /// <summary>
    /// List Item Events
    /// </summary>
    public class ExcelFileEventReceiver : SPItemEventReceiver
    {
        public const string SP_PROPERTYNAME_EXCELIMPORTMAPPINGS = "_smoothics_excelimportmappings";

        /// <summary>
        /// An item was updated.
        /// </summary>
        public override void ItemUpdated(SPItemEventProperties properties)
        {
            base.ItemUpdated(properties);
            DoImportExcelData(properties);
        }

        private void DoImportExcelData(SPItemEventProperties properties)
        {
            var mappingStr = (string)properties.List.RootFolder.Properties[SP_PROPERTYNAME_EXCELIMPORTMAPPINGS];
            if (string.IsNullOrWhiteSpace(mappingStr))
                return;

            var splitted = mappingStr.Split('|');
            var selectedListId = splitted[0];
            var selectedContentTypeId = splitted[1];
            var clearAllDataBeforeImport = bool.Parse(splitted[2]);
            var mappingItemString = splitted[3].Split(';');
            Dictionary<string, MappingItem> mappingItems = new Dictionary<string, MappingItem>();
            for (int i = 0; i < mappingItemString.Length; i++)
            {
                var splittedValues = mappingItemString[i].Split(',');
                mappingItems.Add(splittedValues[0], new MappingItem()
                {
                    ContentTypeColumnStaticName = splittedValues[0],
                    ExcelColumnName = splittedValues[1],
                    ClearContentBeforeImport = bool.Parse(splittedValues[2])
                });
            }

            using (SPSite site = new SPSite(properties.SiteId))
            {
                using (SPWeb thisWeb = site.OpenWeb(properties.RelativeWebUrl))
                {
                    SPList list = thisWeb.Lists[properties.ListId];
                    if (properties.ListItem.FileSystemObjectType == SPFileSystemObjectType.File)
                    {
                        SPFile file = properties.ListItem.File;
                        string fileName = "~/" + file.Url;
                        Guid gFile = file.Item.UniqueId;

                        try
                        {
                            var targetList = thisWeb.Lists[new Guid(selectedListId)];
                            if (clearAllDataBeforeImport)
                                SharepointHelper.DeleteAllItems(site, targetList);

                            var targetContentType = targetList.ContentTypes[new SPContentTypeId(selectedContentTypeId)];
                            Dictionary<string, SPField> keyMappingFields = new Dictionary<string, SPField>();   // f.e. "A" ... SPLookup, "B" ... SPNumber, etc.
                            var targetListItems = targetList.Items;
                            using (var helper = new ExcelHelper(file.OpenBinaryStream()))
                            {
                                int sheetPosition = 1;  //TODO: now it's fixed on only one sheet in a excel workbook -> need to evaluate ALL sheets
                                int maxColumns = helper.GetMaxUsedColumns(sheetPosition);
                                int maxRows = helper.GetMaxUsedRows(sheetPosition);

                                for (int rowNr = 1; rowNr <= maxRows; rowNr++)
                                {
                                    SPListItem newItem = targetListItems.Add();
                                    newItem[SPBuiltInFieldId.ContentTypeId] = targetContentType.Id;

                                    foreach (var keyName in mappingItems.Keys)
                                    {
                                        SPField field = null;
                                        if (!keyMappingFields.TryGetValue(keyName, out field))
                                        {
                                            field = newItem.Fields.TryGetFieldByStaticName(keyName);
                                            keyMappingFields.Add(keyName, field);
                                        }
                                        if (keyMappingFields[keyName] == null)
                                            continue;

                                        SetNewItemValues(ref newItem, thisWeb, field, keyName, helper, sheetPosition, rowNr, mappingItems[keyName].ExcelColumnIndex);
                                    }
                                    newItem.Update();
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //TODO: log error? or do UI Error?
                            properties.Status = SPEventReceiverStatus.CancelWithError;
                        }
                    }
                }
            }
        }

        private void SetNewItemValues(ref SPListItem newItem, SPWeb thisWeb, SPField field, string keyName, ExcelHelper helper, int sheetPosition, int row, int column)
        {
            if (newItem == null)
                return;

            var cellValue = helper.GetCellValue(sheetPosition, row, column);
            switch (field.Type)
            {
                case SPFieldType.Invalid:
                    break;
                case SPFieldType.Integer:
                    newItem[keyName] = helper.GetCellValueAsNumber(sheetPosition, row, column);
                    break;
                case SPFieldType.Text:
                    newItem[keyName] = cellValue;
                    break;
                case SPFieldType.Note:
                    newItem[keyName] = cellValue;
                    break;
                case SPFieldType.DateTime:
                    newItem[keyName] = helper.GetCellValueAsDateTime(sheetPosition, row, column);
                    break;
                case SPFieldType.Counter:
                    break;
                case SPFieldType.Choice:
                    // TODO
                    break;
                case SPFieldType.Lookup:
                    var lookup = (SPFieldLookup)field;
                    var lookupItemValues = SharepointHelper.GetLookupItem(thisWeb, lookup, cellValue);
                    // TODO: logging if result > 1 since we don't know which one to select... => default: we take first item
                    if (lookupItemValues != null && lookupItemValues.Count > 0)
                    {
                        var displayText = lookupItemValues[0][lookup.LookupField] as string;
                        newItem[keyName] = new SPFieldLookupValue(lookupItemValues[0].ID, displayText);
                    }
                    break;
                case SPFieldType.Boolean:
                    newItem[keyName] = helper.GetCellValueAsBoolean(sheetPosition, row, column);
                    break;
                case SPFieldType.Number:
                    newItem[keyName] = helper.GetCellValueAsNumber(sheetPosition, row, column);
                    break;
                case SPFieldType.Currency:
                    newItem[keyName] = helper.GetCellValueAsNumber(sheetPosition, row, column);
                    break;
                case SPFieldType.URL:
                    break;
                case SPFieldType.Computed:
                    break;
                case SPFieldType.Threading:
                    break;
                case SPFieldType.Guid:
                    break;
                case SPFieldType.MultiChoice:
                    var ite = newItem[keyName];
                    break;
                case SPFieldType.GridChoice:
                    break;
                case SPFieldType.Calculated:
                    break;
                case SPFieldType.File:
                    break;
                case SPFieldType.Attachments:
                    break;
                case SPFieldType.User:
                    break;
                case SPFieldType.Recurrence:
                    break;
                case SPFieldType.CrossProjectLink:
                    break;
                case SPFieldType.ModStat:
                    break;
                case SPFieldType.Error:
                    break;
                case SPFieldType.ContentTypeId:
                    break;
                case SPFieldType.PageSeparator:
                    break;
                case SPFieldType.ThreadIndex:
                    break;
                case SPFieldType.WorkflowStatus:
                    break;
                case SPFieldType.AllDayEvent:
                    break;
                case SPFieldType.WorkflowEventType:
                    break;
                case SPFieldType.Geolocation:
                    break;
                case SPFieldType.OutcomeChoice:
                    break;
                case SPFieldType.MaxItems:
                    break;
                default:
                    newItem[keyName] = cellValue;   // is that okay??
                    break;
            }
        }
    }
}