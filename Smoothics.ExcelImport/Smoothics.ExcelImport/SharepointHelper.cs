﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smoothics.ExcelImport
{
    public sealed class SharepointHelper
    {
        public static void DeleteAllItems(SPSite site, SPList list)
        {
            var rounds = (int) Math.Ceiling((decimal) list.ItemCount / 1000);
            for (int i = 0; i < rounds; i++)
            {
                StringBuilder deletebuilder = BatchCommand(list);
                var result = site.RootWeb.ProcessBatchData(deletebuilder.ToString());
            }
        }

        private static StringBuilder BatchCommand(SPList spList)
        {
            StringBuilder deletebuilder = new StringBuilder();
            deletebuilder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Batch>");
            string command = "<Method><SetList Scope=\"Request\">" + spList.ID + "</SetList><SetVar Name=\"ID\">{0}</SetVar><SetVar Name=\"owsfileref\">{1}</SetVar><SetVar Name=\"Cmd\">Delete</SetVar></Method>";

            int line = 0;
            var allItems = spList.Items;
            foreach (SPListItem item in allItems)
            {
                deletebuilder.Append(string.Format(command, item.ID.ToString(), item["FileRef"].ToString()));
                if (line > 1000)   // for performance
                    break;
                line++;
            }

            deletebuilder.Append("</Batch>");
            return deletebuilder;
        }

        public static SPListItemCollection GetLookupItem(SPWeb thisWeb, SPFieldLookup destinationField, string searchValue)
        {
            SPList sourceList = thisWeb.Lists[new Guid(destinationField.LookupList)];
            SPField sourceField = sourceList.Fields.TryGetFieldByStaticName(destinationField.LookupField);
            string query = $"<Where><Eq><FieldRef Name='{sourceField.StaticName}'/><Value Type='Text'>{searchValue}</Value></Eq></Where>";
            return sourceList.GetItems(new SPQuery() { Query = query });
        }
    }
}
